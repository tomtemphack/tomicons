var fs = require('fs');
const fsPromises = fs.promises;
const _path = require('path');

const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

const listDir = async (path) => {
  try {
    return (await (fsPromises.readdir(path))).filter(item => !(/(^|\/)\.[^\/\.]/g).test(item)).map(f => _path.join(path, f));
  } catch (error) {
    console.error('Error occured while reading directory!', error);
  }
}

const readFile = async (path) => {
  try {
    const file = await fsPromises.readFile(path);

    const nameRegEx = /(?<=<title>).*?(?=<)/g 
    let name = nameRegEx.exec(file)[0]

    let nameSplited = name.split('_');
    nameSplited = nameSplited.map((text,index) => index > 0 ? capitalize(text) : text);

    name = nameSplited.join('');
                
    const dRegEx = /(?<= d=").*?(?=")/g
    const d = dRegEx.exec(file)[0]
                
    return {name, d}
  } catch (error) {
    console.log(error, path);
  }
}

const writeIconsObjets = (icons) => {
  let variables = '';

  for(icon of icons) {
    variables = variables.concat(`
  var ${icon.name} = {
    prefix: 'axis',
    iconName: '${icon.name}',
    icon: [24, 24, [], "f0000", "${icon.d}"]
  };
  `)
  }
  return variables;
}

const writeIconExports = (icons) => {
  let exp = '';

  for(icon of icons) {
    exp = exp.concat(`exports.${icon.name} = ${icon.name};
    `);
  }

  return exp;
}

const writeEsExport = (icons) => {
  return `export {${icons.map(i =>  i.name).join(',\n  ')}}`;
}

const main = async () => {
  let svgs = [];
  let icons = [];
  
  const directoryPath = _path.join(__dirname, 'svgs');
  const folders = await listDir(directoryPath);

  for (const folder of folders) {
    const files = await listDir(folder);
    svgs = svgs.concat(files);
  }

  for (const svg of svgs) {
    const obj =  await readFile(svg);
    icons.push(obj);
  }

  fs.writeFile('index.js',
  `(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global['icons'] = {})));
}(this, (function (exports) { 'use strict';

  ${writeIconsObjets(icons)}
  
  ${writeIconExports(icons)}

  Object.defineProperty(exports, '__esModule', { value: true });
})));
  `, function (err) {
    if (err) return console.log(err);
  });

  fs.writeFile('index.es.js', `
${writeIconsObjets(icons)}
${writeEsExport(icons)}
  `, function (err) {
    if (err) return console.log(err);
  })
}

main();


