var addCircleOutlined = {
  prefix: 'axis',
  iconName: 'addCircleOutlined',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm3.5-9H13V8.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V11H8.5a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5H11v2.5a.5.5,0,0,0,.5.5h1a.5.5,0,0,0,.5-.5V13h2.5a.5.5,0,0,0,.5-.5v-1A.5.5,0,0,0,15.5,11Z"]
};

var addCircle = {
  prefix: 'axis',
  iconName: 'addCircle',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm4,10.5a.5.5,0,0,1-.5.5H13v2.5a.5.5,0,0,1-.5.5h-1a.5.5,0,0,1-.5-.5V13H8.5a.5.5,0,0,1-.5-.5v-1a.5.5,0,0,1,.5-.5H11V8.5a.5.5,0,0,1,.5-.5h1a.5.5,0,0,1,.5.5V11h2.5a.5.5,0,0,1,.5.5Z"]
};

var addCircle = {
  prefix: 'axis',
  iconName: 'addCircle',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3Zm0,16H5V5H19ZM8.5,13H11v2.5a.5.5,0,0,0,.5.5h1a.5.5,0,0,0,.5-.5V13h2.5a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5H13V8.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V11H8.5a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,8.5,13Z"]
};

var addSquareOutlined = {
  prefix: 'axis',
  iconName: 'addSquareOutlined',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,3A3,3,0,1,1,9,8,3,3,0,0,1,12,5Zm5.62,11.16a7,7,0,0,1-11.24,0,1,1,0,0,1-.07-1l.21-.44A3,3,0,0,1,9.23,13h5.54a3,3,0,0,1,2.69,1.68l.23.47A1,1,0,0,1,17.62,16.16Z"]
};

var addSquare = {
  prefix: 'axis',
  iconName: 'addSquare',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3Zm-3,9.5a.5.5,0,0,1-.5.5H13v2.5a.5.5,0,0,1-.5.5h-1a.5.5,0,0,1-.5-.5V13H8.5a.5.5,0,0,1-.5-.5v-1a.5.5,0,0,1,.5-.5H11V8.5a.5.5,0,0,1,.5-.5h1a.5.5,0,0,1,.5.5V11h2.5a.5.5,0,0,1,.5.5Z"]
};

var add = {
  prefix: 'axis',
  iconName: 'add',
  icon: [24, 24, [], "f0000", "M19,11.5v1a.5.5,0,0,1-.5.5H13v5.5a.5.5,0,0,1-.5.5h-1a.5.5,0,0,1-.5-.5V13H5.5a.5.5,0,0,1-.5-.5v-1a.5.5,0,0,1,.5-.5H11V5.5a.5.5,0,0,1,.5-.5h1a.5.5,0,0,1,.5.5V11h5.5A.5.5,0,0,1,19,11.5Z"]
};

var appointmentCancelled = {
  prefix: 'axis',
  iconName: 'appointmentCancelled',
  icon: [24, 24, [], "f0000", "M21,6a2,2,0,0,0-2-2H18V2.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V4H8V2.5A.5.5,0,0,0,7.5,2h-1a.5.5,0,0,0-.5.5V4H5A2,2,0,0,0,3,6V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2ZM19,19H5V8H19Zm-5.76-6,1.61-1.62a.48.48,0,0,0,0-.7l-.53-.53a.48.48,0,0,0-.7,0L12,11.76l-1.62-1.61a.48.48,0,0,0-.7,0l-.53.53a.48.48,0,0,0,0,.7L10.76,13,9.15,14.62a.48.48,0,0,0,0,.7l.53.53a.48.48,0,0,0,.7,0L12,14.24l1.62,1.61a.48.48,0,0,0,.7,0l.53-.53a.48.48,0,0,0,0-.7Z"]
};

var appointment = {
  prefix: 'axis',
  iconName: 'appointment',
  icon: [24, 24, [], "f0000", "M19,4H18V2.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V4H8V2.5A.5.5,0,0,0,7.5,2h-1a.5.5,0,0,0-.5.5V4H5A2,2,0,0,0,3,6V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V6A2,2,0,0,0,19,4Zm0,15H5V8H19Zm-8.41-3.15a.48.48,0,0,0,.7,0l4.25-4.24a.51.51,0,0,0,0-.71L15,10.37a.51.51,0,0,0-.71,0l-3.36,3.36L9.7,12.49a.51.51,0,0,0-.71,0L8.46,13a.5.5,0,0,0,0,.7Z"]
};

var archive = {
  prefix: 'axis',
  iconName: 'archive',
  icon: [24, 24, [], "f0000", "M19,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V5C21,3.9,20.1,3,19,3z M15.9,14.3l-3.5,3.5c-0.2,0.2-0.5,0.2-0.7,0c0,0,0,0,0,0l-3.5-3.5c-0.2-0.2-0.2-0.5,0-0.7l0.2-0.2c0.1-0.1,0.2-0.1,0.4-0.1H10V9.5C10,9.2,10.2,9,10.5,9h3C13.8,9,14,9.2,14,9.5v3.8h1.3c0.1,0,0.3,0,0.4,0.1l0.2,0.2C16.1,13.8,16.1,14.1,15.9,14.3z M19,7H5V5h14V7z"]
};

var autorenew = {
  prefix: 'axis',
  iconName: 'autorenew',
  icon: [24, 24, [], "f0000", "M13.06,1.15,16.6,4.68a.51.51,0,0,1,0,.71L13.06,8.92a.5.5,0,0,1-.71,0l-.2-.2A.49.49,0,0,1,12,8.36V6a6,6,0,0,0-6,6,5.94,5.94,0,0,0,.69,2.79.5.5,0,0,1-.09.59l-.73.73a.5.5,0,0,1-.42.14A.52.52,0,0,1,5.08,16,8,8,0,0,1,12,4V1.71a.49.49,0,0,1,.15-.36l.2-.2A.5.5,0,0,1,13.06,1.15ZM10.94,15.08,7.4,18.61a.51.51,0,0,0,0,.71l3.54,3.53a.5.5,0,0,0,.71,0l.2-.2a.49.49,0,0,0,.15-.36V20A8,8,0,0,0,18.92,8a.52.52,0,0,0-.37-.24.5.5,0,0,0-.42.14l-.74.74a.51.51,0,0,0-.09.59A5.83,5.83,0,0,1,18,12a6,6,0,0,1-6,6V15.64a.49.49,0,0,0-.15-.36l-.2-.2A.5.5,0,0,0,10.94,15.08Z"]
};

var blockAds = {
  prefix: 'axis',
  iconName: 'blockAds',
  icon: [24, 24, [], "f0000", "M20.12,7.39,16.61,3.88A3,3,0,0,0,14.49,3h-5a3,3,0,0,0-2.12.88L3.88,7.39A3,3,0,0,0,3,9.51v5a3,3,0,0,0,.88,2.12l3.51,3.51A3,3,0,0,0,9.51,21h5a3,3,0,0,0,2.12-.88l3.51-3.51A3,3,0,0,0,21,14.49v-5A3,3,0,0,0,20.12,7.39ZM5,14.49v-5a1,1,0,0,1,.29-.7L8.81,5.29A1,1,0,0,1,9.51,5h5a1,1,0,0,1,.7.29l1.14,1.14-9.9,9.9L5.29,15.19A1,1,0,0,1,5,14.49Zm14,0a1,1,0,0,1-.29.7l-3.52,3.52a1,1,0,0,1-.7.29h-5a1,1,0,0,1-.7-.29L7.67,17.57l9.9-9.9,1.14,1.14a1,1,0,0,1,.29.7Z"]
};

var bookmarkOutlined = {
  prefix: 'axis',
  iconName: 'bookmarkOutlined',
  icon: [24, 24, [], "f0000", "M18,4V19.19l-4.41-2.75a3,3,0,0,0-3.18,0L6,19.19V4H18m0-2H6A2,2,0,0,0,4,4V21a1,1,0,0,0,1,1,1,1,0,0,0,.53-.15l5.94-3.72a1,1,0,0,1,1.06,0l5.94,3.72A1,1,0,0,0,19,22a1,1,0,0,0,1-1V4a2,2,0,0,0-2-2Z"]
};

var bookmark = {
  prefix: 'axis',
  iconName: 'bookmark',
  icon: [24, 24, [], "f0000", "M20,21a1,1,0,0,1-1.53.85l-5.94-3.72a1,1,0,0,0-1.06,0L5.53,21.85A1,1,0,0,1,4,21V4A2,2,0,0,1,6,2H18a2,2,0,0,1,2,2Z"]
};

var bookmarks_outlined = {
  prefix: 'axis',
  iconName: 'bookmarks_outlined',
  icon: [24, 24, [], "f0000", "9.19l-4.41-2.75a3,3,0,0,0-3.18,0L4,19.19V4M4,2A2,2,0,0,0,2,4V21a1,1,0,0,0,1,1,1,1,0,0,0,.53-.15l5.94-3.72a1,1,0,0,1,1.06,0l5.94,3.72A1,1,0,0,0,17,22a1,1,0,0,0,1-1V4a2,2,0,0,0-2-2ZM21,20a1,1,0,0,0,1-1V5a1,1,0,0,0-1-1H20V20Z"]
};

var bookmarks = {
  prefix: 'axis',
  iconName: 'bookmarks',
  icon: [24, 24, [], "f0000", "M18,21a1,1,0,0,1-1.53.85l-5.94-3.72a1,1,0,0,0-1.06,0L3.53,21.85A1,1,0,0,1,2,21V4A2,2,0,0,1,4,2H16a2,2,0,0,1,2,2ZM21,4H20V20h1a1,1,0,0,0,1-1V5A1,1,0,0,0,21,4Z"]
};

var browserWindow = {
  prefix: 'axis',
  iconName: 'browserWindow',
  icon: [24, 24, [], "f0000", "M20,6m0,2V18H4V8Zm0-4H4A2,2,0,0,0,2,6V18a2,2,0,0,0,2,2H20a2,2,0,0,0,2-2V6a2,2,0,0,0-2-2Z"]
};

var browserWindows = {
  prefix: 'axis',
  iconName: 'browserWindows',
  icon: [24, 24, [], "f0000", "M19,15V5a2,2,0,0,0-2-2H3A2,2,0,0,0,1,5V15a2,2,0,0,0,2,2H17A2,2,0,0,0,19,15ZM3,7H17v8H3ZM21,7V17a2,2,0,0,1-2,2H5a2,2,0,0,0,2,2H19a4,4,0,0,0,4-4V9A2,2,0,0,0,21,7Z"]
};

var cached = {
  prefix: 'axis',
  iconName: 'cached',
  icon: [24, 24, [], "f0000", "M22.85,11.65l-.2.2a.49.49,0,0,1-.36.15H20A8,8,0,0,1,8,18.92a.52.52,0,0,1-.24-.37.5.5,0,0,1,.14-.42l.73-.73a.5.5,0,0,1,.59-.09A5.94,5.94,0,0,0,12,18a6,6,0,0,0,6-6H15.64a.49.49,0,0,1-.36-.15l-.2-.2a.5.5,0,0,1,0-.71L18.61,7.4a.51.51,0,0,1,.71,0l3.53,3.54A.5.5,0,0,1,22.85,11.65Zm-13.93.7-.2-.2A.49.49,0,0,0,8.36,12H6a6,6,0,0,1,6-6,5.83,5.83,0,0,1,2.79.7.51.51,0,0,0,.59-.09l.74-.74a.5.5,0,0,0,.14-.42A.52.52,0,0,0,16,5.08,8,8,0,0,0,4,12H1.71a.49.49,0,0,0-.36.15l-.2.2a.5.5,0,0,0,0,.71L4.68,16.6a.51.51,0,0,0,.71,0l3.53-3.54A.5.5,0,0,0,8.92,12.35Z"]
};

var calendarMonth = {
  prefix: 'axis',
  iconName: 'calendarMonth',
  icon: [24, 24, [], "f0000", "M19,4H18V2.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V4H8V2.5A.5.5,0,0,0,7.5,2h-1a.5.5,0,0,0-.5.5V4H5A2,2,0,0,0,3,6V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V6A2,2,0,0,0,19,4Zm0,15H5V8H19Zm-7.5-7h1a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,11.5,12Zm4,0h1a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,15.5,12Zm-8,0h1a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,7.5,12Zm4,4h1a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,11.5,16Zm-4,0h1a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,7.5,16Z"]
};

var calendarNote = {
  prefix: 'axis',
  iconName: 'calendarNote',
  icon: [24, 24, [], "f0000", "M19,4H18V2.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V4H8V2.5A.5.5,0,0,0,7.5,2h-1a.5.5,0,0,0-.5.5V4H5A2,2,0,0,0,3,6V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V6A2,2,0,0,0,19,4Zm0,15H5V8H19ZM7.5,16h5a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-5a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,7.5,16Zm0-4h9a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-9a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,7.5,12Z"]
};

var calendar = {
  prefix: 'axis',
  iconName: 'calendar',
  icon: [24, 24, [], "f0000", "M19,4H18V2.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V4H8V2.5A.5.5,0,0,0,7.5,2h-1a.5.5,0,0,0-.5.5V4H5A2,2,0,0,0,3,6V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V6A2,2,0,0,0,19,4Zm0,15H5V8H19Z"]
};

var carouselHorizontalOutlined = {
  prefix: 'axis',
  iconName: 'carouselHorizontalOutlined',
  icon: [24, 24, [], "f0000", "M16,6V18H8V6h8m0-2H8A2,2,0,0,0,6,6V18a2,2,0,0,0,2,2h8a2,2,0,0,0,2-2V6a2,2,0,0,0-2-2Zm5,14a1,1,0,0,0,1-1V7a1,1,0,0,0-1-1H20V18ZM3,6A1,1,0,0,0,2,7V17a1,1,0,0,0,1,1H4V6Z"]
};

var carouselHorizontal = {
  prefix: 'axis',
  iconName: 'carouselHorizontal',
  icon: [24, 24, [], "f0000", "M18,18V6a2,2,0,0,0-2-2H8A2,2,0,0,0,6,6V18a2,2,0,0,0,2,2h8A2,2,0,0,0,18,18Zm3,0a1,1,0,0,0,1-1V7a1,1,0,0,0-1-1H20V18ZM3,6A1,1,0,0,0,2,7V17a1,1,0,0,0,1,1H4V6Z"]
};

var carouselVerticalOutlined = {
  prefix: 'axis',
  iconName: 'carouselVerticalOutlined',
  icon: [24, 24, [], "f0000", "M18,8v8H6V8H18m0-2H6A2,2,0,0,0,4,8v8a2,2,0,0,0,2,2H18a2,2,0,0,0,2-2V8a2,2,0,0,0-2-2Zm0-3a1,1,0,0,0-1-1H7A1,1,0,0,0,6,3V4H18ZM6,21a1,1,0,0,0,1,1H17a1,1,0,0,0,1-1V20H6Z"]
};

var carouselVertical = {
  prefix: 'axis',
  iconName: 'carouselVertical',
  icon: [24, 24, [], "f0000", "M18,6H6A2,2,0,0,0,4,8v8a2,2,0,0,0,2,2H18a2,2,0,0,0,2-2V8A2,2,0,0,0,18,6Zm0-3a1,1,0,0,0-1-1H7A1,1,0,0,0,6,3V4H18ZM6,21a1,1,0,0,0,1,1H17a1,1,0,0,0,1-1V20H6Z"]
};

var checkmarkCircleOutlined = {
  prefix: 'axis',
  iconName: 'checkmarkCircleOutlined',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20ZM15.82,8.49a.5.5,0,0,0-.7,0l-4.74,4.74-1.5-1.49a.48.48,0,0,0-.7,0l-.53.53a.5.5,0,0,0,0,.71L10,15.35a.48.48,0,0,0,.7,0l5.62-5.62a.5.5,0,0,0,0-.71Z"]
};

var checkmarkCircle = {
  prefix: 'axis',
  iconName: 'checkmarkCircle',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm4.35,7.73-5.62,5.62a.48.48,0,0,1-.7,0L7.65,13a.5.5,0,0,1,0-.71l.53-.53a.48.48,0,0,1,.7,0l1.5,1.49,4.74-4.74a.5.5,0,0,1,.7,0l.53.53A.5.5,0,0,1,16.35,9.73Z"]
};

var checkmarkSquareOutlined = {
  prefix: 'axis',
  iconName: 'checkmarkSquareOutlined',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3Zm0,16H5V5H19Zm-9-3.65a.48.48,0,0,0,.7,0l5.62-5.62a.5.5,0,0,0,0-.71l-.53-.53a.5.5,0,0,0-.7,0l-4.74,4.74-1.5-1.49a.48.48,0,0,0-.7,0l-.53.53a.5.5,0,0,0,0,.71Z"]
};

var checkmarkSquare = {
  prefix: 'axis',
  iconName: 'checkmarkSquare',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3ZM16.35,9.73l-5.62,5.62a.48.48,0,0,1-.7,0L7.65,13a.5.5,0,0,1,0-.71l.53-.53a.48.48,0,0,1,.7,0l1.5,1.49,4.74-4.74a.5.5,0,0,1,.7,0l.53.53A.5.5,0,0,1,16.35,9.73Z"]
};

var clearCircleOutlined = {
  prefix: 'axis',
  iconName: 'checkmarkSquare',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3ZM16.35,9.73l-5.62,5.62a.48.48,0,0,1-.7,0L7.65,13a.5.5,0,0,1,0-.71l.53-.53a.48.48,0,0,1,.7,0l1.5,1.49,4.74-4.74a.5.5,0,0,1,.7,0l.53.53A.5.5,0,0,1,16.35,9.73Z"]
};

var clearCircle = {
  prefix: 'axis',
  iconName: 'clearCircle',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm3.09,11.86a.48.48,0,0,1,0,.7l-.53.53a.48.48,0,0,1-.7,0L12,13.24l-1.86,1.85a.48.48,0,0,1-.7,0l-.53-.53a.48.48,0,0,1,0-.7L10.76,12,8.91,10.14a.48.48,0,0,1,0-.7l.53-.53a.48.48,0,0,1,.7,0L12,10.76l1.86-1.85a.48.48,0,0,1,.7,0l.53.53a.48.48,0,0,1,0,.7L13.24,12Z"]
};

var clearSquareOutlined = {
  prefix: 'axis',
  iconName: 'clearSquareOutlined',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3Zm0,16H5V5H19ZM8.91,14.56l.53.53a.48.48,0,0,0,.7,0L12,13.24l1.86,1.85a.48.48,0,0,0,.7,0l.53-.53a.48.48,0,0,0,0-.7L13.24,12l1.85-1.86a.48.48,0,0,0,0-.7l-.53-.53a.48.48,0,0,0-.7,0L12,10.76,10.14,8.91a.48.48,0,0,0-.7,0l-.53.53a.48.48,0,0,0,0,.7L10.76,12,8.91,13.86A.48.48,0,0,0,8.91,14.56Z"]
};

var clearSquare = {
  prefix: 'axis',
  iconName: 'clearSquare',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3ZM15.09,13.86a.48.48,0,0,1,0,.7l-.53.53a.48.48,0,0,1-.7,0L12,13.24l-1.86,1.85a.48.48,0,0,1-.7,0l-.53-.53a.48.48,0,0,1,0-.7L10.76,12,8.91,10.14a.48.48,0,0,1,0-.7l.53-.53a.48.48,0,0,1,.7,0L12,10.76l1.86-1.85a.48.48,0,0,1,.7,0l.53.53a.48.48,0,0,1,0,.7L13.24,12Z"]
};

var clear = {
  prefix: 'axis',
  iconName: 'clear',
  icon: [24, 24, [], "f0000", "M17.85,16.44a.5.5,0,0,1,0,.71l-.7.7a.5.5,0,0,1-.71,0L12,13.41,7.56,17.85a.5.5,0,0,1-.71,0l-.7-.7a.5.5,0,0,1,0-.71L10.59,12,6.15,7.56a.5.5,0,0,1,0-.71l.7-.7a.5.5,0,0,1,.71,0L12,10.59l4.44-4.44a.5.5,0,0,1,.71,0l.7.7a.5.5,0,0,1,0,.71L13.41,12Z"]
};

var columnViewOutlined = {
  prefix: 'axis',
  iconName: 'columnViewOutlined',
  icon: [24, 24, [], "f0000", "M20,6V18H15V6h5m0-2H15a2,2,0,0,0-2,2V18a2,2,0,0,0,2,2h5a2,2,0,0,0,2-2V6a2,2,0,0,0-2-2ZM9,6V18H4V6H9M9,4H4A2,2,0,0,0,2,6V18a2,2,0,0,0,2,2H9a2,2,0,0,0,2-2V6A2,2,0,0,0,9,4Z"]
};

var columnView = {
  prefix: 'axis',
  iconName: 'columnView',
  icon: [24, 24, [], "f0000", "M22,18V6a2,2,0,0,0-2-2H15a2,2,0,0,0-2,2V18a2,2,0,0,0,2,2h5A2,2,0,0,0,22,18ZM9,20H4a2,2,0,0,1-2-2V6A2,2,0,0,1,4,4H9a2,2,0,0,1,2,2V18A2,2,0,0,1,9,20Z"]
};

var commandOutlined = {
  prefix: 'axis',
  iconName: 'commandOutlined',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3Zm0,16H5V5H19ZM14.62,8.15a.48.48,0,0,1,.7,0l.53.53a.48.48,0,0,1,0,.7L9.38,15.85a.48.48,0,0,1-.7,0l-.53-.53a.48.48,0,0,1,0-.7Z"]
};

var command = {
  prefix: 'axis',
  iconName: 'command',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3ZM15.85,9.38,9.38,15.85a.48.48,0,0,1-.7,0l-.53-.53a.48.48,0,0,1,0-.7l6.47-6.47a.48.48,0,0,1,.7,0l.53.53A.48.48,0,0,1,15.85,9.38Z"]
};

var dateRange = {
  prefix: 'axis',
  iconName: 'dateRange',
  icon: [24, 24, [], "f0000", "M21,6a2,2,0,0,0-2-2H18V2.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V4H8V2.5A.5.5,0,0,0,7.5,2h-1a.5.5,0,0,0-.5.5V4H5A2,2,0,0,0,3,6V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2ZM19,19H5V8H19Zm-7.5-9a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5h5a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5Zm-4,2h1a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,7.5,12Zm5,4a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-5a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5Zm2.5-.5a.5.5,0,0,0,.5.5h1a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5Z"]
};

var done = {
  prefix: 'axis',
  iconName: 'done',
  icon: [24, 24, [], "f0000", "M19.85,7.25,9.25,17.85a.5.5,0,0,1-.71,0L3.15,12.46a.5.5,0,0,1,0-.71l.7-.7a.5.5,0,0,1,.71,0l4.33,4.33,9.55-9.55a.51.51,0,0,1,.71,0l.7.71A.5.5,0,0,1,19.85,7.25Z"]
};

var drag = {
  prefix: 'axis',
  iconName: 'drag',
  icon: [24, 24, [], "f0000", "M11,6A2,2,0,1,1,9,4,2,2,0,0,1,11,6ZM9,10a2,2,0,1,0,2,2A2,2,0,0,0,9,10Zm0,6a2,2,0,1,0,2,2A2,2,0,0,0,9,16Zm6-8a2,2,0,1,0-2-2A2,2,0,0,0,15,8Zm0,2a2,2,0,1,0,2,2A2,2,0,0,0,15,10Zm0,6a2,2,0,1,0,2,2A2,2,0,0,0,15,16Z"]
};

var ejectCircleOutlined = {
  prefix: 'axis',
  iconName: 'ejectCircleOutlined',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8,8,0,0,1,12,20Zm4.5-6h-9a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5h9a.5.5,0,0,0,.5-.5v-1A.5.5,0,0,0,16.5,14ZM12.35,6.15a.48.48,0,0,0-.7,0l-4.5,4.79a.5.5,0,0,0,0,.71l.2.2a.49.49,0,0,0,.36.15h8.58a.49.49,0,0,0,.36-.15l.2-.2a.5.5,0,0,0,0-.71Z"]
};

var ejectCircle = {
  prefix: 'axis',
  iconName: 'ejectCircle',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm5,13.5a.5.5,0,0,1-.5.5h-9a.5.5,0,0,1-.5-.5v-1a.5.5,0,0,1,.5-.5h9a.5.5,0,0,1,.5.5Zm-.15-3.85-.2.2a.49.49,0,0,1-.36.15H7.71a.49.49,0,0,1-.36-.15l-.2-.2a.5.5,0,0,1,0-.71l4.5-4.79a.48.48,0,0,1,.7,0l4.5,4.79A.5.5,0,0,1,16.85,11.65Z"]
};

var eject = {
  prefix: 'axis',
  iconName: 'eject',
  icon: [24, 24, [], "f0000", "M19,16.5v1a.5.5,0,0,1-.5.5H5.5a.5.5,0,0,1-.5-.5v-1a.5.5,0,0,1,.5-.5h13A.5.5,0,0,1,19,16.5ZM5.35,13.85a.49.49,0,0,0,.36.15H18.29a.49.49,0,0,0,.36-.15l.2-.2a.5.5,0,0,0,0-.71l-6.5-6.79a.48.48,0,0,0-.7,0l-6.5,6.79a.5.5,0,0,0,0,.71Z"]
};

var event = {
  prefix: 'axis',
  iconName: 'event',
  icon: [24, 24, [], "f0000", "M21,6a2,2,0,0,0-2-2H18V2.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V4H8V2.5A.5.5,0,0,0,7.5,2h-1a.5.5,0,0,0-.5.5V4H5A2,2,0,0,0,3,6V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2ZM19,19H5V8H19ZM8,14h2a1,1,0,0,0,1-1V11a1,1,0,0,0-1-1H8a1,1,0,0,0-1,1v2A1,1,0,0,0,8,14Z"]
};

var exitFullscreen11 = {
  prefix: 'axis',
  iconName: 'exitFullscreen11',
  icon: [24, 24, [], "f0000", "M21,6a2,2,0,0,0-2-2H18V2.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V4H8V2.5A.5.5,0,0,0,7.5,2h-1a.5.5,0,0,0-.5.5V4H5A2,2,0,0,0,3,6V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2ZM19,19H5V8H19ZM8,14h2a1,1,0,0,0,1-1V11a1,1,0,0,0-1-1H8a1,1,0,0,0-1,1v2A1,1,0,0,0,8,14Z"]
};

var exitFullscreen43 = {
  prefix: 'axis',
  iconName: 'exitFullscreen43',
  icon: [24, 24, [], "f0000", "M18,9h3.5a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5H19V4.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V8A1,1,0,0,0,18,9ZM6,9A1,1,0,0,0,7,8V4.5A.5.5,0,0,0,6.5,4h-1a.5.5,0,0,0-.5.5V7H2.5a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5ZM19,19.5V17h2.5a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5H18a1,1,0,0,0-1,1v3.5a.5.5,0,0,0,.5.5h1A.5.5,0,0,0,19,19.5ZM5.5,20h1a.5.5,0,0,0,.5-.5V16a1,1,0,0,0-1-1H2.5a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5H5v2.5A.5.5,0,0,0,5.5,20Z"]
};

var exitFullscreen169 = {
  prefix: 'axis',
  iconName: 'exitFullscreen169',
  icon: [24, 24, [], "f0000", "M19,11h3.5a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5H20V6.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V10A1,1,0,0,0,19,11ZM6,10V6.5A.5.5,0,0,0,5.5,6h-1a.5.5,0,0,0-.5.5V9H1.5a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5H5A1,1,0,0,0,6,10Zm14,7.5V15h2.5a.5.5,0,0,0,.5-.5v-1a.5.5,0,0,0-.5-.5H19a1,1,0,0,0-1,1v3.5a.5.5,0,0,0,.5.5h1A.5.5,0,0,0,20,17.5ZM4.5,18h1a.5.5,0,0,0,.5-.5V14a1,1,0,0,0-1-1H1.5a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5H4v2.5A.5.5,0,0,0,4.5,18Z"]
};

var accountCircle = {
  prefix: 'axis',
  iconName: 'accountCircle',
  icon: [24, 24, [], "f0000", "M12,2A10,10,0,1,0,22,12,10,10,0,0,0,12,2Zm0,3A3,3,0,1,1,9,8,3,3,0,0,1,12,5Zm5.62,11.16a7,7,0,0,1-11.24,0,1,1,0,0,1-.07-1l.21-.44A3,3,0,0,1,9.23,13h5.54a3,3,0,0,1,2.69,1.68l.23.47A1,1,0,0,1,17.62,16.16Z"]
};

var accountSquareOutlined = {
  prefix: 'axis',
  iconName: 'accountSquareOutlined',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3Zm0,16H5V5H19Zm-7-7.75A2.25,2.25,0,1,0,9.75,9,2.24,2.24,0,0,0,12,11.25ZM7.75,17h8.5a.75.75,0,0,0,.67-1.09L16,14.1A2,2,0,0,0,14.23,13H9.77A2,2,0,0,0,8,14.1l-.9,1.81A.75.75,0,0,0,7.75,17Z"]
};

var accountSquare = {
  prefix: 'axis',
  iconName: 'accountSquare',
  icon: [24, 24, [], "f0000", "M19,3H5A2,2,0,0,0,3,5V19a2,2,0,0,0,2,2H19a2,2,0,0,0,2-2V5A2,2,0,0,0,19,3ZM12,6A3,3,0,1,1,9,9,3,3,0,0,1,12,6Zm5.25,12H6.75a.75.75,0,0,1-.67-1.09L7,15.1A2,2,0,0,1,8.78,14h6.44A2,2,0,0,1,17,15.1l.91,1.81A.75.75,0,0,1,17.25,18Z"]
};

var groupAdd = {
  prefix: 'axis',
  iconName: 'groupAdd',
  icon: [24, 24, [], "f0000", "M11.89,16.55A1,1,0,0,1,11,18H1a1,1,0,0,1-.89-1.45l1-1.9A3,3,0,0,1,3.74,13H8.26a3,3,0,0,1,2.68,1.65ZM6,11A3,3,0,1,0,3,8,3,3,0,0,0,6,11Zm17.5-1H21V7.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V10H16.5a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5H19v2.5a.5.5,0,0,0,.5.5h1a.5.5,0,0,0,.5-.5V12h2.5a.5.5,0,0,0,.5-.5v-1A.5.5,0,0,0,23.5,10ZM11,11a3,3,0,1,0-.9-5.86,5,5,0,0,1,0,5.72A3,3,0,0,0,11,11Zm4.94,3.65A3,3,0,0,0,13.26,13h-1a4.89,4.89,0,0,1,.48.76l.95,1.89A3,3,0,0,1,13.82,18H16a1,1,0,0,0,.89-1.45Z"]
};

var groupEqual = {
  prefix: 'axis',
  iconName: 'groupEqual',
  icon: [24, 24, [], "f0000", "M5,8a3,3,0,1,1,3,3A3,3,0,0,1,5,8Zm11,3a3,3,0,1,0-3-3A3,3,0,0,0,16,11Zm6.89,6.55-1.44-2.89A3,3,0,0,0,18.77,13h-4a4.89,4.89,0,0,1,.48.76l1.45,2.9A3,3,0,0,1,16.83,19H22A1,1,0,0,0,22.89,17.55Zm-9.44-2.89A3,3,0,0,0,10.76,13H5.24a3,3,0,0,0-2.69,1.66L1.11,17.55A1,1,0,0,0,2,19H14a1,1,0,0,0,.89-1.45Z"]
};

var groupJunior = {
  prefix: 'axis',
  iconName: 'groupJunior',
  icon: [24, 24, [], "f0000", "M16.5,12A2.5,2.5,0,1,0,14,9.5,2.49,2.49,0,0,0,16.5,12ZM9,11A3,3,0,1,0,6,8,3,3,0,0,0,9,11Zm2.76,2a4.91,4.91,0,0,0-1.49,1.76l-.95,1.89A3,3,0,0,0,9.18,19H3a1,1,0,0,1-.89-1.45l1.44-2.89A3,3,0,0,1,6.24,13ZM12,19a1,1,0,0,1-.89-1.45l1-1.9A3,3,0,0,1,14.74,14h3.52a3,3,0,0,1,2.68,1.65l.95,1.9A1,1,0,0,1,21,19Z"]
};

var groupSenior = {
  prefix: 'axis',
  iconName: 'groupSenior',
  icon: [24, 24, [], "f0000", "M14,9.5A2.5,2.5,0,1,1,16.5,12,2.5,2.5,0,0,1,14,9.5ZM9,11A3,3,0,1,0,6,8,3,3,0,0,0,9,11Zm5.45,3.66A3,3,0,0,0,11.76,13H6.24a3,3,0,0,0-2.69,1.66L2.11,17.55A1,1,0,0,0,3,19H15a1,1,0,0,0,.89-1.45Zm7.44,2.89-.95-1.9A3,3,0,0,0,18.26,14H16.35l1.33,2.66A3,3,0,0,1,17.83,19H21A1,1,0,0,0,21.89,17.55Z"]
};

var userAdd = {
  prefix: 'axis',
  iconName: 'userAdd',
  icon: [24, 24, [], "f0000", "M16.89,18.55A1,1,0,0,1,16,20H2a1,1,0,0,1-.89-1.45l1.44-2.89A3,3,0,0,1,5.24,14h7.52a3,3,0,0,1,2.69,1.66ZM9,12A4,4,0,1,0,5,8,4,4,0,0,0,9,12Zm13.5-2H20V7.5a.5.5,0,0,0-.5-.5h-1a.5.5,0,0,0-.5.5V10H15.5a.5.5,0,0,0-.5.5v1a.5.5,0,0,0,.5.5H18v2.5a.5.5,0,0,0,.5.5h1a.5.5,0,0,0,.5-.5V12h2.5a.5.5,0,0,0,.5-.5v-1A.5.5,0,0,0,22.5,10Z"]
};

var userBigOutlined = {
  prefix: 'axis',
  iconName: 'userBigOutlined',
  icon: [24, 24, [], "f0000", "M12,12A5,5,0,1,0,7,7,5,5,0,0,0,12,12Zm0-8A3,3,0,1,1,9,7,3,3,0,0,1,12,4Zm9.89,16.55L20,16.76A5,5,0,0,0,15.53,14H8.47A5,5,0,0,0,4,16.76L2.11,20.55A1,1,0,0,0,3,22H21A1,1,0,0,0,21.89,20.55ZM4.62,20l1.17-2.34A3,3,0,0,1,8.47,16h7.06a3,3,0,0,1,2.68,1.66L19.38,20Z"]
};

var userBig = {
  prefix: 'axis',
  iconName: 'userBig',
  icon: [24, 24, [], "f0000", "M12,12A4,4,0,1,0,8,8,4,4,0,0,0,12,12Zm0-6a2,2,0,1,1-2,2A2,2,0,0,1,12,6Zm7.89,12.55L18,14.66A3,3,0,0,0,15.26,13H8.74a3,3,0,0,0-2.69,1.66L4.11,18.55A1,1,0,0,0,5,20H19A1,1,0,0,0,19.89,18.55ZM6.62,18l1.22-2.45a1,1,0,0,1,.9-.55h6.52a1,1,0,0,1,.9.55L17.38,18Z"]
};

var userOutlined = {
  prefix: 'axis',
  iconName: 'userOutlined',
  icon: [24, 24, [], "f0000", "M12,12A4,4,0,1,0,8,8,4,4,0,0,0,12,12Zm0-6a2,2,0,1,1-2,2A2,2,0,0,1,12,6Zm7.89,12.55L18,14.66A3,3,0,0,0,15.26,13H8.74a3,3,0,0,0-2.69,1.66L4.11,18.55A1,1,0,0,0,5,20H19A1,1,0,0,0,19.89,18.55ZM6.62,18l1.22-2.45a1,1,0,0,1,.9-.55h6.52a1,1,0,0,1,.9.55L17.38,18Z"]
};

var userSwitch = {
  prefix: 'axis',
  iconName: 'userSwitch',
  icon: [24, 24, [], "f0000", "M9,4a3,3,0,1,1,3,3A3,3,0,0,1,9,4ZM7,14H17a1,1,0,0,0,.89-1.45l-.95-1.9A3,3,0,0,0,14.26,9H9.74a3,3,0,0,0-2.68,1.65l-.95,1.9A1,1,0,0,0,7,14Zm11.5,2H14.61a.5.5,0,0,0-.35.85l1.46,1.47a5,5,0,0,1-8.53-2A.5.5,0,0,0,6.71,16h-1a.49.49,0,0,0-.39.19.53.53,0,0,0-.1.42,7,7,0,0,0,12,3.13l1,1a.49.49,0,0,0,.85-.35V16.5A.52.52,0,0,0,18.5,16Z"]
};

var user = {
  prefix: 'axis',
  iconName: 'user',
  icon: [24, 24, [], "M8,8a4,4,0,1,1,4,4A4,4,0,0,1,8,8ZM19.89,18.55l-1.44-2.89A3,3,0,0,0,15.76,14H8.24a3,3,0,0,0-2.69,1.66L4.11,18.55A1,1,0,0,0,5,20H19A1,1,0,0,0,19.89,18.55Z"]
};

var usersSwitch = {
  prefix: 'axis',
  iconName: 'usersSwitch',
  icon: [24, 24, [], "f0000", "M6,4.5A2.5,2.5,0,1,1,8.5,7,2.5,2.5,0,0,1,6,4.5Zm7.89,8.05-1-1.9A3,3,0,0,0,10.26,9H6.74a3,3,0,0,0-2.68,1.65l-.95,1.9A1,1,0,0,0,4,14h9A1,1,0,0,0,13.89,12.55ZM15.5,7A2.5,2.5,0,1,0,13,4.5,2.49,2.49,0,0,0,15.5,7Zm5.39,5.55-.95-1.9A3,3,0,0,0,17.26,9h-3a4.89,4.89,0,0,1,.48.76l.95,1.89A3,3,0,0,1,15.82,14H20A1,1,0,0,0,20.89,12.55ZM17.5,16H13.61a.5.5,0,0,0-.35.85l1.46,1.47a5,5,0,0,1-8.53-2A.5.5,0,0,0,5.71,16h-1a.49.49,0,0,0-.39.19.53.53,0,0,0-.1.42,7,7,0,0,0,12,3.13l1,1a.49.49,0,0,0,.85-.35V16.5A.52.52,0,0,0,17.5,16Z"]
};

export { addCircleOutlined, addCircle, addSquareOutlined, addSquare, add, accountCircle, accountSquareOutlined, accountSquare, groupAdd, groupEqual, groupJunior, groupSenior, userAdd, userBigOutlined, userBig, userOutlined, userSwitch, user, usersSwitch }