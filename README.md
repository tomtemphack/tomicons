# AXIS ICONS 

#### Categories
* [Actions](#actions)
* [Alert](#alert)
* [Arrows](#arrows)
* [Gestures & emotions](#gestures-and-emotions)
* [Health & medical](#health-and-medical)
* [Keyboard](#keyboard)
* [Map & places](#map-and-places)
* [Media & editing](#media-and-editing)
* [Other](#other)
* [Payment](#payment)
* [People & activity](#people-and-activity)
* [Premium](#premium)
* [Social media & tools](#social-media-and-tools)
* [Technology & data](#technology-and-data)
* [Transport](#transport)
* [UI](#ui)
* [User](#user)
------------------------
##### Actions
<table>
<tr>
  <td style="text-align:center;">add_circle_outlined<br><p><img src="svgs/Actions/add_circle_outlined.svg" width="40" title="add_circle_outlined" /><p></td>
  <td style="text-align:center;">add_circle<br><p><img src="svgs/Actions/add_circle.svg" width="40" title="add_circle" /><p></td>
  <td style="text-align:center;">add_square_outlined<br><p><img src="svgs/Actions/add_square_outlined.svg" width="40" title="add_square_outlined" /><p></td>
  <td style="text-align:center;">add_square<br><p><img src="svgs/Actions/add_square.svg" width="40" title="add_square" /><p></td>
  <td style="text-align:center;">add<br><p><img src="svgs/Actions/add.svg" width="40" title="add" /><p></td>
  <td style="text-align:center;">appointment_cancelled<br><p><img src="svgs/Actions/appointment_cancelled.svg" width="40" title="appointment_cancelled" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">appointment<br><p><img src="svgs/Actions/appointment.svg" width="40" title="appointment" /><p></td>
  <td style="text-align:center;">archive<br><p><img src="svgs/Actions/archive.svg" width="40" title="archive" /><p></td>
  <td style="text-align:center;">autorenew<br><p><img src="svgs/Actions/autorenew.svg" width="40" title="autorenew" /><p></td>
  <td style="text-align:center;">bookmark_outlined<br><p><img src="svgs/Actions/bookmark_outlined.svg" width="40" title="bookmark_outlined" /><p></td>
  <td style="text-align:center;">browser_window<br><p><img src="svgs/Actions/browser_window.svg" width="40" title="browser_window" /><p></td>
  <td style="text-align:center;">cached<br><p><img src="svgs/Actions/cached.svg" width="40" title="cached" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">calendar_month<br><p><img src="svgs/Actions/calendar_month.svg" width="40" title="calendar_month" /><p></td>
  <td style="text-align:center;">calendar_note<br><p><img src="svgs/Actions/calendar_note.svg" width="40" title="calendar_note" /><p></td>
  <td style="text-align:center;">calendar<br><p><img src="svgs/Actions/calendar.svg" width="40" title="calendar" /><p></td>
  <td style="text-align:center;">carousel_horizontal_outlined<br><p><img src="svgs/Actions/carousel_horizontal_outlined.svg" width="40" title="carousel_horizontal_outlined" /><p></td>
  <td style="text-align:center;">carousel_horizontal<br><p><img src="svgs/Actions/carousel_horizontal.svg" width="40" title="carousel_horizontal" /><p></td>
  <td style="text-align:center;">carousel_vertical_outlined<br><p><img src="svgs/Actions/carousel_vertical_outlined.svg" width="40" title="carousel_vertical_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">carousel_vertical<br><p><img src="svgs/Actions/carousel_vertical.svg" width="40" title="carousel_vertical" /><p></td>
  <td style="text-align:center;">checkmark_circle_outlined<br><p><img src="svgs/Actions/checkmark_circle_outlined.svg" width="40" title="checkmark_circle_outlined" /><p></td>
  <td style="text-align:center;">checkmark_circle<br><p><img src="svgs/Actions/checkmark_circle.svg" width="40" title="checkmark_circle" /><p></td>
  <td style="text-align:center;">checkmark_square_outlined<br><p><img src="svgs/Actions/checkmark_square_outlined.svg" width="40" title="checkmark_square_outlined" /><p></td>
  <td style="text-align:center;">checkmark_square<br><p><img src="svgs/Actions/checkmark_square.svg" width="40" title="checkmark_square" /><p></td>
  <td style="text-align:center;">clear_circle_outlined<br><p><img src="svgs/Actions/clear_circle_outlined.svg" width="40" title="clear_circle_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">clear_circle<br><p><img src="svgs/Actions/clear_circle.svg" width="40" title="clear_circle" /><p></td>
  <td style="text-align:center;">clear_square<br><p><img src="svgs/Actions/clear_square.svg" width="40" title="clear_square" /><p></td>
  <td style="text-align:center;">clear<br><p><img src="svgs/Actions/clear.svg" width="40" title="clear" /><p></td>
  <td style="text-align:center;">column_view_outlined<br><p><img src="svgs/Actions/column_view_outlined.svg" width="40" title="column_view_outlined" /><p></td>
  <td style="text-align:center;">column_view<br><p><img src="svgs/Actions/column_view.svg" width="40" title="column_view" /><p></td>
  <td style="text-align:center;">command_outlined<br><p><img src="svgs/Actions/command_outlined.svg" width="40" title="command_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">command<br><p><img src="svgs/Actions/command.svg" width="40" title="command" /><p></td>
  <td style="text-align:center;">date_range<br><p><img src="svgs/Actions/date_range.svg" width="40" title="date_range" /><p></td>
  <td style="text-align:center;">done<br><p><img src="svgs/Actions/done.svg" width="40" title="done" /><p></td>
  <td style="text-align:center;">drag<br><p><img src="svgs/Actions/drag.svg" width="40" title="drag" /><p></td>
  <td style="text-align:center;">eject_circle_outlined<br><p><img src="svgs/Actions/eject_circle_outlined.svg" width="40" title="eject_circle_outlined" /><p></td>
  <td style="text-align:center;">eject_circle<br><p><img src="svgs/Actions/eject_circle.svg" width="40" title="eject_circle" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">eject<br><p><img src="svgs/Actions/eject.svg" width="40" title="eject" /><p></td>
  <td style="text-align:center;">event<br><p><img src="svgs/Actions/event.svg" width="40" title="event" /><p></td>
  <td style="text-align:center;">exit_fullscreen_1_1<br><p><img src="svgs/Actions/exit_fullscreen_1_1.svg" width="40" title="exit_fullscreen_1_1" /><p></td>
  <td style="text-align:center;">exit_fullscreen_4_3<br><p><img src="svgs/Actions/exit_fullscreen_4_3.svg" width="40" title="exit_fullscreen_4_3" /><p></td>
  <td style="text-align:center;">exit_fullscreen_16_9<br><p><img src="svgs/Actions/exit_fullscreen_16_9.svg" width="40" title="exit_fullscreen_16_9" /><p></td>
  <td style="text-align:center;">find_replace<br><p><img src="svgs/Actions/find_replace.svg" width="40" title="find_replace" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">first_page<br><p><img src="svgs/Actions/first_page.svg" width="40" title="first_page" /><p></td>
  <td style="text-align:center;">hashtag<br><p><img src="svgs/Actions/hashtag.svg" width="40" title="hashtag" /><p></td>
  <td style="text-align:center;">heart_outlined<br><p><img src="svgs/Actions/heart_outlined.svg" width="40" title="heart_outlined" /><p></td>
  <td style="text-align:center;">heart<br><p><img src="svgs/Actions/heart.svg" width="40" title="heart" /><p></td>
  <td style="text-align:center;">help_outlined<br><p><img src="svgs/Actions/help_outlined.svg" width="40" title="help_outlined" /><p></td>
  <td style="text-align:center;">help<br><p><img src="svgs/Actions/help.svg" width="40" title="help" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">history<br><p><img src="svgs/Actions/history.svg" width="40" title="history" /><p></td>
  <td style="text-align:center;">incognito<br><p><img src="svgs/Actions/incognito.svg" width="40" title="incognito" /><p></td>
  <td style="text-align:center;">info_outined<br><p><img src="svgs/Actions/info_outined.svg" width="40" title="info_outined" /><p></td>
  <td style="text-align:center;">info<br><p><img src="svgs/Actions/info.svg" width="40" title="info" /><p></td>
  <td style="text-align:center;">key<br><p><img src="svgs/Actions/key.svg" width="40" title="key" /><p></td>
  <td style="text-align:center;">label_important<br><p><img src="svgs/Actions/label_important.svg" width="40" title="label_important" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">label_off<br><p><img src="svgs/Actions/label_off.svg" width="40" title="label_off" /><p></td>
  <td style="text-align:center;">label_outlined<br><p><img src="svgs/Actions/label_outlined.svg" width="40" title="label_outlined" /><p></td>
  <td style="text-align:center;">label<br><p><img src="svgs/Actions/label.svg" width="40" title="label" /><p></td>
  <td style="text-align:center;">label_outlined<br><p><img src="svgs/Actions/label_outlined.svg" width="40" title="label_outlined" /><p></td>
  <td style="text-align:center;">label<br><p><img src="svgs/Actions/label.svg" width="40" title="label" /><p></td>
  <td style="text-align:center;">labels_outlined<br><p><img src="svgs/Actions/labels_outlined.svg" width="40" title="labels_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">labels<br><p><img src="svgs/Actions/labels.svg" width="40" title="labels" /><p></td>
  <td style="text-align:center;">last_page<br><p><img src="svgs/Actions/last_page.svg" width="40" title="last_page" /><p></td>
  <td style="text-align:center;">layout<br><p><img src="svgs/Actions/layout.svg" width="40" title="layout" /><p></td>
  <td style="text-align:center;">move_page<br><p><img src="svgs/Actions/move_page.svg" width="40" title="move_page" /><p></td>
  <td style="text-align:center;">multi_direction_diagonal<br><p><img src="svgs/Actions/multi_direction_diagonal.svg" width="40" title="multi_direction_diagonal" /><p></td>
  <td style="text-align:center;">multi_direction<br><p><img src="svgs/Actions/multi_direction.svg" width="40" title="multi_direction" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">notebook_bookmarked<br><p><img src="svgs/Actions/notebook_bookmarked.svg" width="40" title="notebook_bookmarked" /><p></td>
  <td style="text-align:center;">notice_outlined<br><p><img src="svgs/Actions/notice_outlined.svg" width="40" title="notice_outlined" /><p></td>
  <td style="text-align:center;">notice<br><p><img src="svgs/Actions/notice.svg" width="40" title="notice" /><p></td>
  <td style="text-align:center;">open_in_new<br><p><img src="svgs/Actions/open_in_new.svg" width="40" title="open_in_new" /><p></td>
  <td style="text-align:center;">pages_outlined<br><p><img src="svgs/Actions/pages_outlined.svg" width="40" title="pages_outlined" /><p></td>
  <td style="text-align:center;">pages<br><p><img src="svgs/Actions/pages.svg" width="40" title="pages" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">publish<br><p><img src="svgs/Actions/publish.svg" width="40" title="publish" /><p></td>
  <td style="text-align:center;">record_screen<br><p><img src="svgs/Actions/record_screen.svg" width="40" title="record_screen" /><p></td>
  <td style="text-align:center;">refresh<br><p><img src="svgs/Actions/refresh.svg" width="40" title="refresh" /><p></td>
  <td style="text-align:center;">remaining_time<br><p><img src="svgs/Actions/remaining_time.svg" width="40" title="remaining_time" /><p></td>
  <td style="text-align:center;">remove_circle_outlined<br><p><img src="svgs/Actions/remove_circle_outlined.svg" width="40" title="remove_circle_outlined" /><p></td>
  <td style="text-align:center;">remove_circle<br><p><img src="svgs/Actions/remove_circle.svg" width="40" title="remove_circle" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">remove_square_outlined<br><p><img src="svgs/Actions/remove_square_outlined.svg" width="40" title="remove_square_outlined" /><p></td>
  <td style="text-align:center;">remove_square<br><p><img src="svgs/Actions/remove_square.svg" width="40" title="remove_square" /><p></td>
  <td style="text-align:center;">remove<br><p><img src="svgs/Actions/remove.svg" width="40" title="remove" /><p></td>
  <td style="text-align:center;">restore<br><p><img src="svgs/Actions/restore.svg" width="40" title="restore" /><p></td>
  <td style="text-align:center;">search<br><p><img src="svgs/Actions/search.svg" width="40" title="search" /><p></td>
  <td style="text-align:center;">settings_back<br><p><img src="svgs/Actions/settings_back.svg" width="40" title="settings_back" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">shopping_basket_add<br><p><img src="svgs/Actions/shopping_basket_add.svg" width="40" title="shopping_basket_add" /><p></td>
  <td style="text-align:center;">shopping_basket_clear<br><p><img src="svgs/Actions/shopping_basket_clear.svg" width="40" title="shopping_basket_clear" /><p></td>
  <td style="text-align:center;">shopping_basket_outlined<br><p><img src="svgs/Actions/shopping_basket_outlined.svg" width="40" title="shopping_basket_outlined" /><p></td>
  <td style="text-align:center;">shopping_basket_remove<br><p><img src="svgs/Actions/shopping_basket_remove.svg" width="40" title="shopping_basket_remove" /><p></td>
  <td style="text-align:center;">shopping_basket<br><p><img src="svgs/Actions/shopping_basket.svg" width="40" title="shopping_basket" /><p></td>
  <td style="text-align:center;">shopping_cart_add<br><p><img src="svgs/Actions/shopping_cart_add.svg" width="40" title="shopping_cart_add" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">shopping_cart_off<br><p><img src="svgs/Actions/shopping_cart_off.svg" width="40" title="shopping_cart_off" /><p></td>
  <td style="text-align:center;">shopping_cart_outlined<br><p><img src="svgs/Actions/shopping_cart_outlined.svg" width="40" title="shopping_cart_outlined" /><p></td>
  <td style="text-align:center;">shopping_cart_remove<br><p><img src="svgs/Actions/shopping_cart_remove.svg" width="40" title="shopping_cart_remove" /><p></td>
  <td style="text-align:center;">shopping_cart<br><p><img src="svgs/Actions/shopping_cart.svg" width="40" title="shopping_cart" /><p></td>
  <td style="text-align:center;">snooze_notification<br><p><img src="svgs/Actions/snooze_notification.svg" width="40" title="snooze_notification" /><p></td>
  <td style="text-align:center;">snooze<br><p><img src="svgs/Actions/snooze.svg" width="40" title="snooze" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">sort<br><p><img src="svgs/Actions/sort.svg" width="40" title="sort" /><p></td>
  <td style="text-align:center;">star_half<br><p><img src="svgs/Actions/star_half.svg" width="40" title="star_half" /><p></td>
  <td style="text-align:center;">star_outlined<br><p><img src="svgs/Actions/star_outlined.svg" width="40" title="star_outlined" /><p></td>
  <td style="text-align:center;">star<br><p><img src="svgs/Actions/star.svg" width="40" title="star" /><p></td>
  <td style="text-align:center;">subscribe<br><p><img src="svgs/Actions/subscribe.svg" width="40" title="subscribe" /><p></td>
  <td style="text-align:center;">support<br><p><img src="svgs/Actions/support.svg" width="40" title="support" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">sync_off<br><p><img src="svgs/Actions/sync_off.svg" width="40" title="sync_off" /><p></td>
  <td style="text-align:center;">sync<br><p><img src="svgs/Actions/sync.svg" width="40" title="sync" /><p></td>
  <td style="text-align:center;">tab<br><p><img src="svgs/Actions/tab.svg" width="40" title="tab" /><p></td>
  <td style="text-align:center;">tabs<br><p><img src="svgs/Actions/tabs.svg" width="40" title="tabs" /><p></td>
  <td style="text-align:center;">time_20_s<br><p><img src="svgs/Actions/time_20_s.svg" width="40" title="time_20_s" /><p></td>
  <td style="text-align:center;">time_30_s<br><p><img src="svgs/Actions/time_30_s.svg" width="40" title="time_30_s" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">time_40_s<br><p><img src="svgs/Actions/time_40_s.svg" width="40" title="time_40_s" /><p></td>
  <td style="text-align:center;">time_40_s<br><p><img src="svgs/Actions/time_40_s.svg" width="40" title="time_40_s" /><p></td>
  <td style="text-align:center;">timer<br><p><img src="svgs/Actions/timer.svg" width="40" title="timer" /><p></td>
  <td style="text-align:center;">time<br><p><img src="svgs/Actions/time.svg" width="40" title="time" /><p></td>
  <td style="text-align:center;">timer_20_s<br><p><img src="svgs/Actions/timer_20_s.svg" width="40" title="timer_20_s" /><p></td>
  <td style="text-align:center;">timer_30_s<br><p><img src="svgs/Actions/timer_30_s.svg" width="40" title="timer_30_s" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">timer_40_s<br><p><img src="svgs/Actions/timer_40_s.svg" width="40" title="timer_40_s" /><p></td>
  <td style="text-align:center;">timer_off<br><p><img src="svgs/Actions/timer_off.svg" width="40" title="timer_off" /><p></td>
  <td style="text-align:center;">timer<br><p><img src="svgs/Actions/timer.svg" width="40" title="timer" /><p></td>
  <td style="text-align:center;">turn_off<br><p><img src="svgs/Actions/turn_off.svg" width="40" title="turn_off" /><p></td>
  <td style="text-align:center;">unarchive<br><p><img src="svgs/Actions/unarchive.svg" width="40" title="unarchive" /><p></td>
  <td style="text-align:center;">update<br><p><img src="svgs/Actions/update.svg" width="40" title="update" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">vibrations_off<br><p><img src="svgs/Actions/vibrations_off.svg" width="40" title="vibrations_off" /><p></td>
  <td style="text-align:center;">vibrations<br><p><img src="svgs/Actions/vibrations.svg" width="40" title="vibrations" /><p></td>
  <td style="text-align:center;">watch_later<br><p><img src="svgs/Actions/watch_later.svg" width="40" title="watch_later" /><p></td>
  <td style="text-align:center;">youtube_search<br><p><img src="svgs/Actions/youtube_search.svg" width="40" title="youtube_search" /><p></td>
</tr>
</table>

##### Alert
<table>
<tr>
  <td style="text-align:center;">battery_alert<br><p><img src="svgs/Alert/battery_alert.svg" width="40" title="battery_alert" /><p></td>
  <td style="text-align:center;">error_outlined<br><p><img src="svgs/Alert/error_outlined.svg" width="40" title="error_outlined" /><p></td>
  <td style="text-align:center;">error<br><p><img src="svgs/Alert/error.svg" width="40" title="error" /><p></td>
  <td style="text-align:center;">message_failed_outlined<br><p><img src="svgs/Alert/message_failed_outlined.svg" width="40" title="message_failed_outlined" /><p></td>
  <td style="text-align:center;">new_release_outlined<br><p><img src="svgs/Alert/new_release_outlined.svg" width="40" title="new_release_outlined" /><p></td>
  <td style="text-align:center;">new_release<br><p><img src="svgs/Alert/new_release.svg" width="40" title="new_release" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">notification_add<br><p><img src="svgs/Alert/notification_add.svg" width="40" title="notification_add" /><p></td>
  <td style="text-align:center;">notifications_alert<br><p><img src="svgs/Alert/notifications_alert.svg" width="40" title="notifications_alert" /><p></td>
  <td style="text-align:center;">notifications_off<br><p><img src="svgs/Alert/notifications_off.svg" width="40" title="notifications_off" /><p></td>
  <td style="text-align:center;">notifications_on_outlined<br><p><img src="svgs/Alert/notifications_on_outlined.svg" width="40" title="notifications_on_outlined" /><p></td>
  <td style="text-align:center;">notifications<br><p><img src="svgs/Alert/notifications.svg" width="40" title="notifications" /><p></td>
  <td style="text-align:center;">report_outlined<br><p><img src="svgs/Alert/report_outlined.svg" width="40" title="report_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">report<br><p><img src="svgs/Alert/report.svg" width="40" title="report" /><p></td>
  <td style="text-align:center;">security_warning_outlined<br><p><img src="svgs/Alert/security_warning_outlined.svg" width="40" title="security_warning_outlined" /><p></td>
  <td style="text-align:center;">security_warning<br><p><img src="svgs/Alert/security_warning.svg" width="40" title="security_warning" /><p></td>
  <td style="text-align:center;">warning_outlined<br><p><img src="svgs/Alert/warning_outlined.svg" width="40" title="warning_outlined" /><p></td>
  <td style="text-align:center;">notifications<br><p><img src="svgs/Alert/notifications.svg" width="40" title="notifications" /><p></td>
  <td style="text-align:center;">warning<br><p><img src="svgs/Alert/warning.svg" width="40" title="warning" /><p></td>
</tr>
</table> 

##### Arrows
<table>
<tr>
  <td style="text-align:center;">arrow_backward<br><p><img src="svgs/Arrows/arrow_backward.svg" width="40" title="arrow_backward" /><p></td>
  <td style="text-align:center;">arrow_doublesided_horizontal<br><p><img src="svgs/Arrows/arrow_doublesided_horizontal.svg" width="40" title="arrow_doublesided_horizontal" /><p></td>
  <td style="text-align:center;">arrow_doublesided_vertical<br><p><img src="svgs/Arrows/arrow_doublesided_vertical.svg" width="40" title="arrow_doublesided_vertical" /><p></td>
  <td style="text-align:center;">arrow_drop_down_circle_outlined<br><p><img src="svgs/Arrows/arrow_drop_down_circle_outlined.svg" width="40" title="arrow_drop_down_circle_outlined" /><p></td>
  <td style="text-align:center;">arrow_drop_down_circle<br><p><img src="svgs/Arrows/arrow_drop_down_circle.svg" width="40" title="arrow_drop_down_circle" /><p></td>
  <td style="text-align:center;">arrow_drop_up_circle_outlined<br><p><img src="svgs/Arrows/arrow_drop_up_circle_outlined.svg" width="40" title="arrow_drop_up_circle_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">arrow_drop_up_circle<br><p><img src="svgs/Arrows/arrow_drop_up_circle.svg" width="40" title="arrow_drop_up_circle" /><p></td>
  <td style="text-align:center;">arrow_forward<br><p><img src="svgs/Arrows/arrow_forward.svg" width="40" title="arrow_forward" /><p></td>
  <td style="text-align:center;">arrow_large_backward_outlined<br><p><img src="svgs/Arrows/arrow_large_backward_outlined.svg" width="40" title="arrow_large_backward_outlined" /><p></td>
  <td style="text-align:center;">arrow_large_backward<br><p><img src="svgs/Arrows/arrow_large_backward.svg" width="40" title="arrow_large_backward" /><p></td>
  <td style="text-align:center;">arrow_large_downward_outlined<br><p><img src="svgs/Arrows/arrow_large_downward_outlined.svg" width="40" title="arrow_large_downward_outlined" /><p></td>
  <td style="text-align:center;">arrow_large_downward<br><p><img src="svgs/Arrows/arrow_large_downward.svg" width="40" title="arrow_large_downward" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">arrow_large_forward_outlined<br><p><img src="svgs/Arrows/arrow_large_forward_outlined.svg" width="40" title="arrow_large_forward_outlined" /><p></td>
  <td style="text-align:center;">arrow_large_forward<br><p><img src="svgs/Arrows/arrow_large_forward.svg" width="40" title="arrow_large_forward" /><p></td>
  <td style="text-align:center;">arrow_large_upward_outlined<br><p><img src="svgs/Arrows/arrow_large_upward_outlined.svg" width="40" title="arrow_large_upward_outlined" /><p></td>
  <td style="text-align:center;">arrow_large_upward<br><p><img src="svgs/Arrows/arrow_large_upward.svg" width="40" title="arrow_large_upward" /><p></td>
  <td style="text-align:center;">arrow_left_circle_outlined<br><p><img src="svgs/Arrows/arrow_left_circle_outlined.svg" width="40" title="arrow_left_circle_outlined" /><p></td>
  <td style="text-align:center;">arrow_left_circle<br><p><img src="svgs/Arrows/arrow_left_circle.svg" width="40" title="arrow_left_circle" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">arrow_long_down<br><p><img src="svgs/Arrows/arrow_long_down.svg" width="40" title="arrow_long_down" /><p></td>
  <td style="text-align:center;">arrow_long_left<br><p><img src="svgs/Arrows/arrow_long_left.svg" width="40" title="arrow_long_left" /><p></td>
  <td style="text-align:center;">arrow_long_right<br><p><img src="svgs/Arrows/arrow_long_right.svg" width="40" title="arrow_long_right" /><p></td>
  <td style="text-align:center;">arrow_long_up<br><p><img src="svgs/Arrows/arrow_long_up.svg" width="40" title="arrow_long_up" /><p></td>
  <td style="text-align:center;">arrow_right_circle_outlined<br><p><img src="svgs/Arrows/arrow_right_circle_outlined.svg" width="40" title="arrow_right_circle_outlined" /><p></td>
  <td style="text-align:center;">arrow_right_circle<br><p><img src="svgs/Arrows/arrow_right_circle.svg" width="40" title="arrow_right_circle" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">arrow_upward<br><p><img src="svgs/Arrows/arrow_upward.svg" width="40" title="arrow_upward" /><p></td>
  <td style="text-align:center;">back_ui<br><p><img src="svgs/Arrows/back_ui.svg" width="40" title="back_ui" /><p></td>
  <td style="text-align:center;">caret_down<br><p><img src="svgs/Arrows/caret_down.svg" width="40" title="caret_down" /><p></td>
  <td style="text-align:center;">caret_left<br><p><img src="svgs/Arrows/caret_left.svg" width="40" title="caret_left" /><p></td>
  <td style="text-align:center;">caret_right<br><p><img src="svgs/Arrows/caret_right.svg" width="40" title="caret_right" /><p></td>
  <td style="text-align:center;">caret_up<br><p><img src="svgs/Arrows/caret_up.svg" width="40" title="caret_up" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">chevron_down<br><p><img src="svgs/Arrows/chevron_down.svg" width="40" title="chevron_down" /><p></td>
  <td style="text-align:center;">chevron_left<br><p><img src="svgs/Arrows/chevron_left.svg" width="40" title="chevron_left" /><p></td>
  <td style="text-align:center;">chevron_right<br><p><img src="svgs/Arrows/chevron_right.svg" width="40" title="chevron_right" /><p></td>
  <td style="text-align:center;">chevron_up<br><p><img src="svgs/Arrows/chevron_up.svg" width="40" title="chevron_up" /><p></td>
  <td style="text-align:center;">collapse_all<br><p><img src="svgs/Arrows/collapse_all.svg" width="40" title="collapse_all" /><p></td>
  <td style="text-align:center;">double_caret_horizontal<br><p><img src="svgs/Arrows/double_caret_horizontal.svg" width="40" title="double_caret_horizontal" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">double_caret_vertical<br><p><img src="svgs/Arrows/double_caret_vertical.svg" width="40" title="double_caret_vertical" /><p></td>
  <td style="text-align:center;">down_ui<br><p><img src="svgs/Arrows/down_ui.svg" width="40" title="down_ui" /><p></td>
  <td style="text-align:center;">expand_all<br><p><img src="svgs/Arrows/expand_all.svg" width="40" title="expand_all" /><p></td>
  <td style="text-align:center;">next_ui<br><p><img src="svgs/Arrows/next_ui.svg" width="40" title="next_ui" /><p></td>
  <td style="text-align:center;">pull_right_left<br><p><img src="svgs/Arrows/pull_right_left.svg" width="40" title="pull_right_left" /><p></td>
  <td style="text-align:center;">subdirectory_left<br><p><img src="svgs/Arrows/subdirectory_left.svg" width="40" title="subdirectory_left" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">subdirectory_right<br><p><img src="svgs/Arrows/subdirectory_right.svg" width="40" title="subdirectory_right" /><p></td>
  <td style="text-align:center;">up_ui<br><p><img src="svgs/Arrows/up_ui.svg" width="40" title="up_ui" /><p></td>
</tr>
</table> 

##### Gestures and emotions
<table>
<tr>
  <td style="text-align:center;">clap<br><p><img src="svgs/Gestures & emotions/clap.svg" width="40" title="clap" /><p></td>
  <td style="text-align:center;">expressionless<br><p><img src="svgs/Gestures & emotions/expressionless.svg" width="40" title="expressionless" /><p></td>
  <td style="text-align:center;">face_female<br><p><img src="svgs/Gestures & emotions/face_female.svg" width="40" title="face_female" /><p></td>
  <td style="text-align:center;">face_male<br><p><img src="svgs/Gestures & emotions/face_male.svg" width="40" title="face_male" /><p></td>
  <td style="text-align:center;">frowning<br><p><img src="svgs/Gestures & emotions/frowning.svg" width="40" title="frowning" /><p></td>
  <td style="text-align:center;">gesture_hand_1f<br><p><img src="svgs/Gestures & emotions/gesture_hand_1f.svg" width="40" title="gesture_hand_1f" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">gesture_hand_2f<br><p><img src="svgs/Gestures & emotions/gesture_hand_2f.svg" width="40" title="gesture_hand_2f" /><p></td>
  <td style="text-align:center;">gesture_hover_1f<br><p><img src="svgs/Gestures & emotions/gesture_hover_1f.svg" width="40" title="gesture_hover_1f" /><p></td>
  <td style="text-align:center;">gesture_scroll_down_2f<br><p><img src="svgs/Gestures & emotions/gesture_scroll_down_2f.svg" width="40" title="gesture_scroll_down_2f" /><p></td>
  <td style="text-align:center;">gesture_scroll_down<br><p><img src="svgs/Gestures & emotions/gesture_scroll_down.svg" width="40" title="gesture_scroll_down" /><p></td>
  <td style="text-align:center;">gesture_scroll_up_2f<br><p><img src="svgs/Gestures & emotions/gesture_scroll_up_2f.svg" width="40" title="gesture_scroll_up_2f" /><p></td>
  <td style="text-align:center;">gesture_scroll_up_down_2f<br><p><img src="svgs/Gestures & emotions/gesture_scroll_up_down_2f.svg" width="40" title="gesture_scroll_up_down_2f" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">gesture_scroll_up_down<br><p><img src="svgs/Gestures & emotions/gesture_scroll_up_down.svg" width="40" title="gesture_scroll_up_down" /><p></td>
  <td style="text-align:center;">gesture_scroll_up<br><p><img src="svgs/Gestures & emotions/gesture_scroll_up.svg" width="40" title="gesture_scroll_up" /><p></td>
  <td style="text-align:center;">gesture_swipe_left_2f<br><p><img src="svgs/Gestures & emotions/gesture_swipe_left_2f.svg" width="40" title="gesture_swipe_left_2f" /><p></td>
  <td style="text-align:center;">gesture_swipe_left_right_2f<br><p><img src="svgs/Gestures & emotions/gesture_swipe_left_right_2f.svg" width="40" title="gesture_swipe_left_right_2f" /><p></td>
  <td style="text-align:center;">gesture_scroll_up_2f<br><p><img src="svgs/Gestures & emotions/gesture_scroll_up_2f.svg" width="40" title="gesture_scroll_up_2f" /><p></td>
  <td style="text-align:center;">gesture_swipe_left_right<br><p><img src="svgs/Gestures & emotions/gesture_swipe_left_right.svg" width="40" title="gesture_swipe_left_right" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">gesture_swipe_left<br><p><img src="svgs/Gestures & emotions/gesture_swipe_left.svg" width="40" title="gesture_swipe_left" /><p></td>
  <td style="text-align:center;">gesture_swipe_right_2f<br><p><img src="svgs/Gestures & emotions/gesture_swipe_right_2f.svg" width="40" title="gesture_swipe_right_2f" /><p></td>
  <td style="text-align:center;">gesture_swipe_right<br><p><img src="svgs/Gestures & emotions/gesture_swipe_right.svg" width="40" title="gesture_swipe_right" /><p></td>
  <td style="text-align:center;">hand_basic<br><p><img src="svgs/Gestures & emotions/hand_basic.svg" width="40" title="hand_basic" /><p></td>
  <td style="text-align:center;">hand_draw<br><p><img src="svgs/Gestures & emotions/hand_draw.svg" width="40" title="hand_draw" /><p></td>
  <td style="text-align:center;">hand_metal_horns<br><p><img src="svgs/Gestures & emotions/hand_metal_horns.svg" width="40" title="hand_metal_horns" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">gesture_swipe_left<br><p><img src="svgs/Gestures & emotions/gesture_swipe_left.svg" width="40" title="gesture_swipe_left" /><p></td>
  <td style="text-align:center;">gesture_swipe_right_2f<br><p><img src="svgs/Gestures & emotions/gesture_swipe_right_2f.svg" width="40" title="gesture_swipe_right_2f" /><p></td>
  <td style="text-align:center;">hand_middle_finger<br><p><img src="svgs/Gestures & emotions/hand_middle_finger.svg" width="40" title="hand_middle_finger" /><p></td>
  <td style="text-align:center;">hand_ok<br><p><img src="svgs/Gestures & emotions/hand_ok.svg" width="40" title="hand_ok" /><p></td>
  <td style="text-align:center;">hand_piece<br><p><img src="svgs/Gestures & emotions/hand_piece.svg" width="40" title="hand_piece" /><p></td>
  <td style="text-align:center;">hand_wave<br><p><img src="svgs/Gestures & emotions/hand_wave.svg" width="40" title="hand_wave" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">gesture_swipe_left<br><p><img src="svgs/Gestures & emotions/gesture_swipe_left.svg" width="40" title="gesture_swipe_left" /><p></td>
  <td style="text-align:center;">gesture_swipe_right_2f<br><p><img src="svgs/Gestures & emotions/gesture_swipe_right_2f.svg" width="40" title="gesture_swipe_right_2f" /><p></td>
  <td style="text-align:center;">neutral_face<br><p><img src="svgs/Gestures & emotions/neutral_face.svg" width="40" title="neutral_face" /><p></td>
  <td style="text-align:center;">protection<br><p><img src="svgs/Gestures & emotions/protection.svg" width="40" title="protection" /><p></td>
  <td style="text-align:center;">sad<br><p><img src="svgs/Gestures & emotions/sad.svg" width="40" title="sad" /><p></td>
  <td style="text-align:center;">slap<br><p><img src="svgs/Gestures & emotions/slap.svg" width="40" title="slap" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">slightly_smilling<br><p><img src="svgs/Gestures & emotions/slightly_smilling.svg" width="40" title="slightly_smilling" /><p></td>
  <td style="text-align:center;">smile<br><p><img src="svgs/Gestures & emotions/smile.svg" width="40" title="smile" /><p></td>
  <td style="text-align:center;">thumbs_down<br><p><img src="svgs/Gestures & emotions/thumbs_down.svg" width="40" title="thumbs_downs" /><p></td>
  <td style="text-align:center;">thumbs_up_down<br><p><img src="svgs/Gestures & emotions/thumbs_up_down.svg" width="40" title="thumbs_up_down" /><p></td>
  <td style="text-align:center;">thumbs_up<br><p><img src="svgs/Gestures & emotions/thumbs_up.svg" width="40" title="thumbs_up" /><p></td>
  <td style="text-align:center;">wink<br><p><img src="svgs/Gestures & emotions/wink.svg" width="40" title="wink" /><p></td>
</tr>
</table> 

##### Health and medical
<table>
<tr>
  <td style="text-align:center;">blood_transfusion<br><p><img src="svgs/Health & medical/blood_transfusion.svg" width="40" title="blood_transfusion" /><p></td>
  <td style="text-align:center;">corona_virus_stop<br><p><img src="svgs/Health & medical/corona_virus_stop.svg" width="40" title="corona_virus_stop" /><p></td>
  <td style="text-align:center;">corona_virus<br><p><img src="svgs/Health & medical/corona_virus.svg" width="40" title="corona_virus" /><p></td>
  <td style="text-align:center;">cover_safety_glasses<br><p><img src="svgs/Health & medical/cover_safety_glasses.svg" width="40" title="cover_safety_glasses" /><p></td>
  <td style="text-align:center;">disinfectant<br><p><img src="svgs/Health & medical/disinfectant.svg" width="40" title="disinfectant" /><p></td>
  <td style="text-align:center;">face_mask_outlined<br><p><img src="svgs/Health & medical/face_mask_outlined.svg" width="40" title="face_mask_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">face_mask<br><p><img src="svgs/Health & medical/face_mask.svg" width="40" title="face_mask" /><p></td>
  <td style="text-align:center;">glove<br><p><img src="svgs/Health & medical/glove.svg" width="40" title="glove" /><p></td>
  <td style="text-align:center;">hand_disinfection<br><p><img src="svgs/Health & medical/hand_disinfection.svg" width="40" title="hand_disinfection" /><p></td>
  <td style="text-align:center;">lungs<br><p><img src="svgs/Health & medical/lungs.svg" width="40" title="lungs" /><p></td>
  <td style="text-align:center;">microscope<br><p><img src="svgs/Health & medical/microscope.svg" width="40" title="microscope" /><p></td>
  <td style="text-align:center;">quarantine_outlined<br><p><img src="svgs/Health & medical/quarantine_outlined.svg" width="40" title="quarantine_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">respirator<br><p><img src="svgs/Health & medical/respirator.svg" width="40" title="respirator" /><p></td>
  <td style="text-align:center;">saline<br><p><img src="svgs/Health & medical/saline.svg" width="40" title="saline" /><p></td>
  <td style="text-align:center;">soap<br><p><img src="svgs/Health & medical/soap.svg" width="40" title="soap" /><p></td>
  <td style="text-align:center;">thermometer<br><p><img src="svgs/Health & medical/thermometer.svg" width="40" title="thermometer" /><p></td>
  <td style="text-align:center;">vaccine<br><p><img src="svgs/Health & medical/vaccine.svg" width="40" title="vaccine" /><p></td>
  <td style="text-align:center;">washing_hands<br><p><img src="svgs/Health & medical/washing_hands.svg" width="40" title="washing_hands" /><p></td>
</tr>
</table> 

##### Keyboard
<table>
<tr>
  <td style="text-align:center;">alt<br><p><img src="svgs/Keyboard/alt.svg" width="40" title="alt" /><p></td>
  <td style="text-align:center;">backspace_outlined<br><p><img src="svgs/Keyboard/backspace_outlined.svg" width="40" title="backspace_outlined" /><p></td>
  <td style="text-align:center;">backspace<br><p><img src="svgs/Keyboard/backspace.svg" width="40" title="backspace" /><p></td>
  <td style="text-align:center;">caps_lock<br><p><img src="svgs/Keyboard/caps_lock.svg" width="40" title="caps_lock" /><p></td>
  <td style="text-align:center;">command_key<br><p><img src="svgs/Keyboard/command_key.svg" width="40" title="command_key" /><p></td>
  <td style="text-align:center;">enter<br><p><img src="svgs/Keyboard/enter.svg" width="40" title="enter" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">keyboard_brightness_1<br><p><img src="svgs/Keyboard/keyboard_brightness_1.svg" width="40" title="keyboard_brightness_1" /><p></td>
  <td style="text-align:center;">keyboard_brightness_2<br><p><img src="svgs/Keyboard/keyboard_brightness_2.svg" width="40" title="keyboard_brightness_2" /><p></td>
  <td style="text-align:center;">space_bar<br><p><img src="svgs/Keyboard/space_bar.svg" width="40" title="space_bar" /><p></td>
  <td style="text-align:center;">tab_key<br><p><img src="svgs/Keyboard/tab_key.svg" width="40" title="tab_key" /><p></td>
</tr>
</table> 

##### Map and places
<table>
<tr>
  <td style="text-align:center;">360_rotate<br><p><img src="svgs/Map & places/360_rotate.svg" width="40" title="360_rotate" /><p></td>
  <td style="text-align:center;">beijing<br><p><img src="svgs/Map & places/beijing.svg" width="40" title="beijing" /><p></td>
  <td style="text-align:center;">bratislava<br><p><img src="svgs/Map & places/bratislava.svg" width="40" title="bratislava" /><p></td>
  <td style="text-align:center;">broadleaf_tree_outlined<br><p><img src="svgs/Map & places/broadleaf_tree_outlined.svg" width="40" title="broadleaf_tree_outlined" /><p></td>
  <td style="text-align:center;">broadleaf_tree<br><p><img src="svgs/Map & places/broadleaf_tree.svg" width="40" title="broadleaf_tree" /><p></td>
  <td style="text-align:center;">cafe_outlined<br><p><img src="svgs/Map & places/cafe_outlined.svg" width="40" title="cafe_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">cafe<br><p><img src="svgs/Map & places/cafe.svg" width="40" title="cafe" /><p></td>
  <td style="text-align:center;">cairo<br><p><img src="svgs/Map & places/cairo.svg" width="40" title="cairo" /><p></td>
  <td style="text-align:center;">castle<br><p><img src="svgs/Map & places/castle.svg" width="40" title="castle" /><p></td>
  <td style="text-align:center;">city<br><p><img src="svgs/Map & places/city.svg" width="40" title="city" /><p></td>
  <td style="text-align:center;">clinic<br><p><img src="svgs/Map & places/clinic.svg" width="40" title="clinic" /><p></td>
  <td style="text-align:center;">company<br><p><img src="svgs/Map & places/company.svg" width="40" title="company" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">conifer_outlined<br><p><img src="svgs/Map & places/conifer_outlined.svg" width="40" title="conifer_outlined" /><p></td>
  <td style="text-align:center;">conifer<br><p><img src="svgs/Map & places/conifer.svg" width="40" title="conifer" /><p></td>
  <td style="text-align:center;">direction<br><p><img src="svgs/Map & places/direction.svg" width="40" title="direction" /><p></td>
  <td style="text-align:center;">dubai<br><p><img src="svgs/Map & places/dubai.svg" width="40" title="dubai" /><p></td>
  <td style="text-align:center;">earth_east<br><p><img src="svgs/Map & places/earth_east.svg" width="40" title="earth_east" /><p></td>
  <td style="text-align:center;">earth_west<br><p><img src="svgs/Map & places/earth_west.svg" width="40" title="earth_west" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">education_outlined<br><p><img src="svgs/Map & places/education_outlined.svg" width="40" title="education_outlined" /><p></td>
  <td style="text-align:center;">education<br><p><img src="svgs/Map & places/education.svg" width="40" title="education" /><p></td>
  <td style="text-align:center;">factory<br><p><img src="svgs/Map & places/factory.svg" width="40" title="factory" /><p></td>
  <td style="text-align:center;">gas_station<br><p><img src="svgs/Map & places/gas_station.svg" width="40" title="gas_station" /><p></td>
  <td style="text-align:center;">globe<br><p><img src="svgs/Map & places/globe.svg" width="40" title="globe" /><p></td>
  <td style="text-align:center;">gym<br><p><img src="svgs/Map & places/gym.svg" width="40" title="gym" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">hospital<br><p><img src="svgs/Map & places/hospital.svg" width="40" title="hospital" /><p></td>
  <td style="text-align:center;">hotel<br><p><img src="svgs/Map & places/hotel.svg" width="40" title="hotel" /><p></td>
  <td style="text-align:center;">location_off<br><p><img src="svgs/Map & places/location_off.svg" width="40" title="location_off" /><p></td>
  <td style="text-align:center;">location_search<br><p><img src="svgs/Map & places/location_search.svg" width="40" title="location_search" /><p></td>
  <td style="text-align:center;">machu_picchu<br><p><img src="svgs/Map & places/machu_picchu.svg" width="40" title="machu_picchu" /><p></td>
  <td style="text-align:center;">map_outlined<br><p><img src="svgs/Map & places/map_outlined.svg" width="40" title="map_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">map_zoom_out<br><p><img src="svgs/Map & places/map_zoom_out.svg" width="40" title="map_zoom_out" /><p></td>
  <td style="text-align:center;">map<br><p><img src="svgs/Map & places/map.svg" width="40" title="map" /><p></td>
  <td style="text-align:center;">museum<br><p><img src="svgs/Map & places/museum.svg" width="40" title="museum" /><p></td>
  <td style="text-align:center;">my_location<br><p><img src="svgs/Map & places/my_location.svg" width="40" title="my_location" /><p></td>
  <td style="text-align:center;">navigate_outlined<br><p><img src="svgs/Map & places/navigate_outlined.svg" width="40" title="navigate_outlined" /><p></td>
  <td style="text-align:center;">navigate<br><p><img src="svgs/Map & places/navigate.svg" width="40" title="navigate" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">neighborhood<br><p><img src="svgs/Map & places/neighborhood.svg" width="40" title="neighborhood" /><p></td>
  <td style="text-align:center;">new_york<br><p><img src="svgs/Map & places/new_york.svg" width="40" title="new_york" /><p></td>
  <td style="text-align:center;">paris<br><p><img src="svgs/Map & places/paris.svg" width="40" title="paris" /><p></td>
  <td style="text-align:center;">parking_outlined<br><p><img src="svgs/Map & places/parking_outlined.svg" width="40" title="parking_outlined" /><p></td>
  <td style="text-align:center;">parking<br><p><img src="svgs/Map & places/parking.svg" width="40" title="parking" /><p></td>
  <td style="text-align:center;">pharmacy_outlined<br><p><img src="svgs/Map & places/pharmacy_outlined.svg" width="40" title="pharmacy_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">pharmacy<br><p><img src="svgs/Map & places/pharmacy.svg" width="40" title="pharmacy" /><p></td>
  <td style="text-align:center;">pin_outlined<br><p><img src="svgs/Map & places/pin_outlined.svg" width="40" title="pin_outlined" /><p></td>
  <td style="text-align:center;">pin<br><p><img src="svgs/Map & places/pin.svg" width="40" title="pin" /><p></td>
  <td style="text-align:center;">poi_add_outlined<br><p><img src="svgs/Map & places/poi_add_outlined.svg" width="40" title="poi_add_outlined" /><p></td>
  <td style="text-align:center;">poi_add<br><p><img src="svgs/Map & places/poi_add.svg" width="40" title="poi_add" /><p></td>
  <td style="text-align:center;">poi_delete_outlined<br><p><img src="svgs/Map & places/poi_delete_outlined.svg" width="40" title="poi_delete_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">poi_delete<br><p><img src="svgs/Map & places/poi_delete.svg" width="40" title="poi_delete" /><p></td>
  <td style="text-align:center;">poi_outlined<br><p><img src="svgs/Map & places/poi_outlined.svg" width="40" title="poi_outlined" /><p></td>
  <td style="text-align:center;">poi_remove_outlined<br><p><img src="svgs/Map & places/poi_remove_outlined.svg" width="40" title="poi_remove_outlined" /><p></td>
  <td style="text-align:center;">poi_remove<br><p><img src="svgs/Map & places/poi_remove.svg" width="40" title="poi_remove" /><p></td>
  <td style="text-align:center;">poi_user<br><p><img src="svgs/Map & places/poi_user.svg" width="40" title="poi_user" /><p></td>
  <td style="text-align:center;">poi<br><p><img src="svgs/Map & places/poi.svg" width="40" title="poi" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">pyramid<br><p><img src="svgs/Map & places/pyramid.svg" width="40" title="pyramid" /><p></td>
  <td style="text-align:center;">restaurant<br><p><img src="svgs/Map & places/restaurant.svg" width="40" title="restaurant" /><p></td>
  <td style="text-align:center;">rome<br><p><img src="svgs/Map & places/rome.svg" width="40" title="rome" /><p></td>
  <td style="text-align:center;">route_dashed<br><p><img src="svgs/Map & places/route_dashed.svg" width="40" title="route_dashed" /><p></td>
  <td style="text-align:center;">route<br><p><img src="svgs/Map & places/route.svg" width="40" title="route" /><p></td>
  <td style="text-align:center;">san_francisco<br><p><img src="svgs/Map & places/san_francisco.svg" width="40" title="san_francisco" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">shop_outlined<br><p><img src="svgs/Map & places/shop_outlined.svg" width="40" title="shop_outlined" /><p></td>
  <td style="text-align:center;">shop<br><p><img src="svgs/Map & places/shop.svg" width="40" title="shop" /><p></td>
  <td style="text-align:center;">shopping_icon<br><p><img src="svgs/Map & places/shopping_icon.svg" width="40" title="shopping_icon" /><p></td>
  <td style="text-align:center;">shopping<br><p><img src="svgs/Map & places/shopping.svg" width="40" title="shopping" /><p></td>
  <td style="text-align:center;">singapore<br><p><img src="svgs/Map & places/singapore.svg" width="40" title="singapore" /><p></td>
  <td style="text-align:center;">spa_outlined<br><p><img src="svgs/Map & places/spa_outlined.svg" width="40" title="spa_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">spa<br><p><img src="svgs/Map & places/spa.svg" width="40" title="spa" /><p></td>
  <td style="text-align:center;">sydney<br><p><img src="svgs/Map & places/sydney.svg" width="40" title="sydney" /><p></td>
  <td style="text-align:center;">theatre<br><p><img src="svgs/Map & places/theatre.svg" width="40" title="theatre" /><p></td>
  <td style="text-align:center;">toilets<br><p><img src="svgs/Map & places/toilets.svg" width="40" title="toilets" /><p></td>
  <td style="text-align:center;">trencin<br><p><img src="svgs/Map & places/trencin.svg" width="40" title="trencin" /><p></td>
  <td style="text-align:center;">zoom_in<br><p><img src="svgs/Map & places/zoom_in.svg" width="40" title="zoom_in" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">zoom_out<br><p><img src="svgs/Map & places/zoom_out.svg" width="40" title="zoom_out" /><p></td>
</tr>
</table> 

##### Media and editing
<table>
<tr>
  <td style="text-align:center;">add_photo<br><p><img src="svgs/Media & editing/add_photo.svg" width="40" title="add_photo" /><p></td>
  <td style="text-align:center;">adjust<br><p><img src="svgs/Media & editing/adjust.svg" width="40" title="adjust" /><p></td>
  <td style="text-align:center;">align_to_bottom_outlined<br><p><img src="svgs/Media & editing/align_to_bottom_outlined.svg" width="40" title="align_to_bottom_outlined" /><p></td>
  <td style="text-align:center;">align_to_bottom_vs<br><p><img src="svgs/Media & editing/align_to_bottom_vs.svg" width="40" title="align_to_bottom_vs" /><p></td>
  <td style="text-align:center;">align_to_bottom<br><p><img src="svgs/Media & editing/align_to_bottom.svg" width="40" title="align_to_bottom" /><p></td>
  <td style="text-align:center;">align_to_center_outlined<br><p><img src="svgs/Media & editing/align_to_center_outlined.svg" width="40" title="align_to_center_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">align_to_center<br><p><img src="svgs/Media & editing/align_to_center.svg" width="40" title="align_to_center" /><p></td>
  <td style="text-align:center;">align_to_left_outlined<br><p><img src="svgs/Media & editing/align_to_left_outlined.svg" width="40" title="align_to_left_outlined" /><p></td>
  <td style="text-align:center;">align_to_left_vs<br><p><img src="svgs/Media & editing/align_to_left_vs.svg" width="40" title="align_to_left_vs" /><p></td>
  <td style="text-align:center;">align_to_left<br><p><img src="svgs/Media & editing/align_to_left.svg" width="40" title="align_to_left" /><p></td>
  <td style="text-align:center;">align_to_middle_outlined<br><p><img src="svgs/Media & editing/align_to_middle_outlined.svg" width="40" title="align_to_middle_outlined" /><p></td>
  <td style="text-align:center;">align_to_right_outlined<br><p><img src="svgs/Media & editing/align_to_right_outlined.svg" width="40" title="align_to_right_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">align_to_right_vs<br><p><img src="svgs/Media & editing/align_to_right_vs.svg" width="40" title="align_to_right_vs" /><p></td>
  <td style="text-align:center;">align_to_right<br><p><img src="svgs/Media & editing/align_to_right.svg" width="40" title="align_to_right" /><p></td>
  <td style="text-align:center;">align_to_top_outlined<br><p><img src="svgs/Media & editing/align_to_top_outlined.svg" width="40" title="align_to_top_outlined" /><p></td>
  <td style="text-align:center;">align_to_top_vs<br><p><img src="svgs/Media & editing/align_to_top_vs.svg" width="40" title="align_to_top_vs" /><p></td>
  <td style="text-align:center;">align_to_top<br><p><img src="svgs/Media & editing/align_to_top.svg" width="40" title="align_to_top" /><p></td>
  <td style="text-align:center;">artboard_outlined<br><p><img src="svgs/Media & editing/artboard_outlined.svg" width="40" title="artboard_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">artboard<br><p><img src="svgs/Media & editing/artboard.svg" width="40" title="artboard" /><p></td>
  <td style="text-align:center;">black_white<br><p><img src="svgs/Media & editing/black_white.svg" width="40" title="black_white" /><p></td>
  <td style="text-align:center;">blend_tool<br><p><img src="svgs/Media & editing/blend_tool.svg" width="40" title="blend_tool" /><p></td>
  <td style="text-align:center;">bold<br><p><img src="svgs/Media & editing/bold.svg" width="40" title="bold" /><p></td>
  <td style="text-align:center;">brightness_1_outlined<br><p><img src="svgs/Media & editing/brightness_1_outlined.svg" width="40" title="brightness_1_outlined" /><p></td>
  <td style="text-align:center;">brightness_1<br><p><img src="svgs/Media & editing/brightness_1.svg" width="40" title="brightness_1" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">brightness_2_outlined<br><p><img src="svgs/Media & editing/brightness_2_outlined.svg" width="40" title="brightness_2_outlined" /><p></td>
  <td style="text-align:center;">brightness_2<br><p><img src="svgs/Media & editing/brightness_2.svg" width="40" title="brightness_2" /><p></td>
  <td style="text-align:center;">brightness_3<br><p><img src="svgs/Media & editing/brightness_3.svg" width="40" title="brightness_3" /><p></td>
  <td style="text-align:center;">bring_forward<br><p><img src="svgs/Media & editing/bring_forward.svg" width="40" title="bring_forward" /><p></td>
  <td style="text-align:center;">bring_to_front<br><p><img src="svgs/Media & editing/bring_to_front.svg" width="40" title="bring_to_front" /><p></td>
  <td style="text-align:center;">brush<br><p><img src="svgs/Media & editing/brush.svg" width="40" title="brush" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">camera_enhance<br><p><img src="svgs/Media & editing/camera_enhance.svg" width="40" title="camera_enhance" /><p></td>
  <td style="text-align:center;">camera_roll<br><p><img src="svgs/Media & editing/camera_roll.svg" width="40" title="camera_roll" /><p></td>
  <td style="text-align:center;">canvas_graphics<br><p><img src="svgs/Media & editing/canvas_graphics.svg" width="40" title="canvas_graphics" /><p></td>
  <td style="text-align:center;">canvas_text<br><p><img src="svgs/Media & editing/canvas_text.svg" width="40" title="canvas_text" /><p></td>
  <td style="text-align:center;">canvas<br><p><img src="svgs/Media & editing/canvas.svg" width="40" title="canvas" /><p></td>
  <td style="text-align:center;">color_off_outlined<br><p><img src="svgs/Media & editing/color_off_outlined.svg" width="40" title="color_off_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">color_off<br><p><img src="svgs/Media & editing/color_off.svg" width="40" title="color_off" /><p></td>
  <td style="text-align:center;">color_outlined<br><p><img src="svgs/Media & editing/color_outlined.svg" width="40" title="color_outlined" /><p></td>
  <td style="text-align:center;">color_picker_empty<br><p><img src="svgs/Media & editing/color_picker_empty.svg" width="40" title="color_picker_empty" /><p></td>
  <td style="text-align:center;">color_picker_point<br><p><img src="svgs/Media & editing/color_picker_point.svg" width="40" title="color_picker_point" /><p></td>
  <td style="text-align:center;">canvas<br><p><img src="svgs/Media & editing/canvas.svg" width="40" title="canvas" /><p></td>
  <td style="text-align:center;">color_picker<br><p><img src="svgs/Media & editing/color_picker.svg" width="40" title="color_picker" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">color<br><p><img src="svgs/Media & editing/color.svg" width="40" title="color" /><p></td>
  <td style="text-align:center;">copy<br><p><img src="svgs/Media & editing/copy.svg" width="40" title="copy" /><p></td>
  <td style="text-align:center;">crop<br><p><img src="svgs/Media & editing/crop.svg" width="40" title="crop" /><p></td>
  <td style="text-align:center;">cut_in_half<br><p><img src="svgs/Media & editing/cut_in_half.svg" width="40" title="cut_in_half" /><p></td>
  <td style="text-align:center;">cut<br><p><img src="svgs/Media & editing/cut.svg" width="40" title="cut" /><p></td>
  <td style="text-align:center;">difference<br><p><img src="svgs/Media & editing/difference.svg" width="40" title="difference" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">eraser<br><p><img src="svgs/Media & editing/eraser.svg" width="40" title="eraser" /><p></td>
  <td style="text-align:center;">exposure<br><p><img src="svgs/Media & editing/exposure.svg" width="40" title="exposure" /><p></td>
  <td style="text-align:center;">flare<br><p><img src="svgs/Media & editing/flare.svg" width="40" title="flare" /><p></td>
  <td style="text-align:center;">flash_off<br><p><img src="svgs/Media & editing/flash_off.svg" width="40" title="flash_off" /><p></td>
  <td style="text-align:center;">flash<br><p><img src="svgs/Media & editing/flash.svg" width="40" title="flash" /><p></td>
  <td style="text-align:center;">focus<br><p><img src="svgs/Media & editing/focus.svg" width="40" title="focus" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">format_1_1<br><p><img src="svgs/Media & editing/format_1_1.svg" width="40" title="format_1_1" /><p></td>
  <td style="text-align:center;">format_4_3<br><p><img src="svgs/Media & editing/format_4_3.svg" width="40" title="format_4_3" /><p></td>
  <td style="text-align:center;">format_16_9<br><p><img src="svgs/Media & editing/format_16_9.svg" width="40" title="format_16_9" /><p></td>
  <td style="text-align:center;">formats<br><p><img src="svgs/Media & editing/formats.svg" width="40" title="formats" /><p></td>
  <td style="text-align:center;">free_transform<br><p><img src="svgs/Media & editing/free_transform.svg" width="40" title="free_transform" /><p></td>
  <td style="text-align:center;">fullscreen_1_1<br><p><img src="svgs/Media & editing/fullscreen_1_1.svg" width="40" title="fullscreen_1_1" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">fullscreen_4_3<br><p><img src="svgs/Media & editing/fullscreen_4_3.svg" width="40" title="fullscreen_4_3" /><p></td>
  <td style="text-align:center;">fullscreen_16_9<br><p><img src="svgs/Media & editing/fullscreen_16_9.svg" width="40" title="fullscreen_16_9" /><p></td>
  <td style="text-align:center;">gesture<br><p><img src="svgs/Media & editing/gesture.svg" width="40" title="gesture" /><p></td>
  <td style="text-align:center;">gradient<br><p><img src="svgs/Media & editing/gradient.svg" width="40" title="gradient" /><p></td>
  <td style="text-align:center;">grid<br><p><img src="svgs/Media & editing/grid.svg" width="40" title="grid" /><p></td>
  <td style="text-align:center;">image_rotate_left<br><p><img src="svgs/Media & editing/image_rotate_left.svg" width="40" title="image_rotate_left" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">image_rotate_right<br><p><img src="svgs/Media & editing/image_rotate_right.svg" width="40" title="image_rotate_right" /><p></td>
  <td style="text-align:center;">image<br><p><img src="svgs/Media & editing/image.svg" width="40" title="image" /><p></td>
  <td style="text-align:center;">intersect<br><p><img src="svgs/Media & editing/intersect.svg" width="40" title="intersect" /><p></td>
  <td style="text-align:center;">invert_colors_off<br><p><img src="svgs/Media & editing/invert_colors_off.svg" width="40" title="invert_colors_off" /><p></td>
  <td style="text-align:center;">invert_colors<br><p><img src="svgs/Media & editing/invert_colors.svg" width="40" title="invert_colors" /><p></td>
  <td style="text-align:center;">invert<br><p><img src="svgs/Media & editing/invert.svg" width="40" title="invert" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">italic<br><p><img src="svgs/Media & editing/italic.svg" width="40" title="italic" /><p></td>
  <td style="text-align:center;">keyframe_bezier_in<br><p><img src="svgs/Media & editing/keyframe_bezier_in.svg" width="40" title="keyframe_bezier_in" /><p></td>
  <td style="text-align:center;">keyframe_bezier_out<br><p><img src="svgs/Media & editing/keyframe_bezier_out.svg" width="40" title="keyframe_bezier_out" /><p></td>
  <td style="text-align:center;">keyframe_cont_bezier<br><p><img src="svgs/Media & editing/keyframe_cont_bezier.svg" width="40" title="keyframe_cont_bezier" /><p></td>
  <td style="text-align:center;">keyframe_linear_in<br><p><img src="svgs/Media & editing/keyframe_linear_in.svg" width="40" title="keyframe_linear_in" /><p></td>
  <td style="text-align:center;">keyframe_linear_out<br><p><img src="svgs/Media & editing/keyframe_linear_out.svg" width="40" title="keyframe_linear_out" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">knife<br><p><img src="svgs/Media & editing/knife.svg" width="40" title="knife" /><p></td>
  <td style="text-align:center;">lasso<br><p><img src="svgs/Media & editing/lasso.svg" width="40" title="lasso" /><p></td>
  <td style="text-align:center;">layers_off<br><p><img src="svgs/Media & editing/layers_off.svg" width="40" title="layers_off" /><p></td>
  <td style="text-align:center;">layers_outlined<br><p><img src="svgs/Media & editing/layers_outlined.svg" width="40" title="layers_outlined" /><p></td>
  <td style="text-align:center;">layers<br><p><img src="svgs/Media & editing/layers.svg" width="40" title="layers" /><p></td>
  <td style="text-align:center;">ligature<br><p><img src="svgs/Media & editing/ligature.svg" width="40" title="ligature" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">macro_outlined<br><p><img src="svgs/Media & editing/macro_outlined.svg" width="40" title="macro_outlined" /><p></td>
  <td style="text-align:center;">macro<br><p><img src="svgs/Media & editing/macro.svg" width="40" title="macro" /><p></td>
  <td style="text-align:center;">magic_wand<br><p><img src="svgs/Media & editing/magic_wand.svg" width="40" title="magic_wand" /><p></td>
  <td style="text-align:center;">marker_outlined<br><p><img src="svgs/Media & editing/marker_outlined.svg" width="40" title="marker_outlined" /><p></td>
  <td style="text-align:center;">marker<br><p><img src="svgs/Media & editing/marker.svg" width="40" title="marker" /><p></td>
  <td style="text-align:center;">mask_vs<br><p><img src="svgs/Media & editing/mask_vs.svg" width="40" title="mask_vs" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">mask<br><p><img src="svgs/Media & editing/mask.svg" width="40" title="mask" /><p></td>
  <td style="text-align:center;">material<br><p><img src="svgs/Media & editing/material.svg" width="40" title="material" /><p></td>
  <td style="text-align:center;">media_photo<br><p><img src="svgs/Media & editing/media_photo.svg" width="40" title="media_photo" /><p></td>
  <td style="text-align:center;">mesh_grid<br><p><img src="svgs/Media & editing/mesh_grid.svg" width="40" title="mesh_grid" /><p></td>
  <td style="text-align:center;">mouse_cursor_outlined<br><p><img src="svgs/Media & editing/mouse_cursor_outlined.svg" width="40" title="mouse_cursor_outlineds" /><p></td>
  <td style="text-align:center;">mouse_cursor<br><p><img src="svgs/Media & editing/mouse_cursor.svg" width="40" title="mouse_cursor" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">opacity<br><p><img src="svgs/Media & editing/opacity.svg" width="40" title="opacity" /><p></td>
  <td style="text-align:center;">paint_bucket<br><p><img src="svgs/Media & editing/paint_bucket.svg" width="40" title="paint_bucket" /><p></td>
  <td style="text-align:center;">paint_outlined<br><p><img src="svgs/Media & editing/paint_outlined.svg" width="40" title="paint_outlined" /><p></td>
  <td style="text-align:center;">paint<br><p><img src="svgs/Media & editing/paint.svg" width="40" title="paint" /><p></td>
  <td style="text-align:center;">palette_outlined<br><p><img src="svgs/Media & editing/palette_outlined.svg" width="40" title="palette_outlined" /><p></td>
  <td style="text-align:center;">palette<br><p><img src="svgs/Media & editing/palette.svg" width="40" title="palette" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">pan_tool<br><p><img src="svgs/Media & editing/pan_tool.svg" width="40" title="pan_tool" /><p></td>
  <td style="text-align:center;">panorama_image<br><p><img src="svgs/Media & editing/panorama_image.svg" width="40" title="panorama_image" /><p></td>
  <td style="text-align:center;">panorama_outlined<br><p><img src="svgs/Media & editing/panorama_outlined.svg" width="40" title="panorama_outlined" /><p></td>
  <td style="text-align:center;">panorama_vs_outlined<br><p><img src="svgs/Media & editing/panorama_vs_outlined.svg" width="40" title="panorama_vs_outlined" /><p></td>
  <td style="text-align:center;">panorama_vs<br><p><img src="svgs/Media & editing/panorama_vs.svg" width="40" title="panorama_vs" /><p></td>
  <td style="text-align:center;">panorama<br><p><img src="svgs/Media & editing/panorama.svg" width="40" title="panorama" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">paste<br><p><img src="svgs/Media & editing/paste.svg" width="40" title="paste" /><p></td>
  <td style="text-align:center;">pattern<br><p><img src="svgs/Media & editing/pattern.svg" width="40" title="pattern" /><p></td>
  <td style="text-align:center;">pen<br><p><img src="svgs/Media & editing/pen.svg" width="40" title="pen" /><p></td>
  <td style="text-align:center;">photo_album<br><p><img src="svgs/Media & editing/photo_album.svg" width="40" title="photo_album" /><p></td>
  <td style="text-align:center;">photo_filter<br><p><img src="svgs/Media & editing/photo_filter.svg" width="40" title="photo_filter" /><p></td>
  <td style="text-align:center;">photo_gallery_outlined<br><p><img src="svgs/Media & editing/photo_gallery_outlined.svg" width="40" title="photo_gallery_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">photo_gallery<br><p><img src="svgs/Media & editing/photo_gallery.svg" width="40" title="photo_gallery" /><p></td>
  <td style="text-align:center;">photo_landscape_outlined<br><p><img src="svgs/Media & editing/photo_landscape_outlined.svg" width="40" title="photo_landscape_outlined" /><p></td>
  <td style="text-align:center;">photo_landscape<br><p><img src="svgs/Media & editing/photo_landscape.svg" width="40" title="photo_landscape" /><p></td>
  <td style="text-align:center;">photo_square_outlined<br><p><img src="svgs/Media & editing/photo_square_outlined.svg" width="40" title="photo_square_outlined" /><p></td>
  <td style="text-align:center;">photo_square<br><p><img src="svgs/Media & editing/photo_square.svg" width="40" title="photo_square" /><p></td>
  <td style="text-align:center;">podcast<br><p><img src="svgs/Media & editing/podcast.svg" width="40" title="podcast" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">print_disabled<br><p><img src="svgs/Media & editing/print_disabled.svg" width="40" title="print_disabled" /><p></td>
  <td style="text-align:center;">print<br><p><img src="svgs/Media & editing/print.svg" width="40" title="print" /><p></td>
  <td style="text-align:center;">redo<br><p><img src="svgs/Media & editing/redo.svg" width="40" title="redo" /><p></td>
  <td style="text-align:center;">reflect_horizontal<br><p><img src="svgs/Media & editing/reflect_horizontal.svg" width="40" title="reflect_horizontal" /><p></td>
  <td style="text-align:center;">reflect_vertical<br><p><img src="svgs/Media & editing/reflect_vertical.svg" width="40" title="reflect_vertical" /><p></td>
  <td style="text-align:center;">rounded_corner<br><p><img src="svgs/Media & editing/rounded_corner.svg" width="40" title="rounded_corner" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">ruler<br><p><img src="svgs/Media & editing/ruler.svg" width="40" title="ruler" /><p></td>
  <td style="text-align:center;">save_outlined<br><p><img src="svgs/Media & editing/save_outlined.svg" width="40" title="save_outlined" /><p></td>
  <td style="text-align:center;">save<br><p><img src="svgs/Media & editing/save.svg" width="40" title="save" /><p></td>
  <td style="text-align:center;">scan_document_icon<br><p><img src="svgs/Media & editing/scan_document_icon.svg" width="40" title="scan_document_icon" /><p></td>
  <td style="text-align:center;">scan_document<br><p><img src="svgs/Media & editing/scan_document.svg" width="40" title="scan_document" /><p></td>
  <td style="text-align:center;">selection<br><p><img src="svgs/Media & editing/selection.svg" width="40" title="selection" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">send_backward<br><p><img src="svgs/Media & editing/send_backward.svg" width="40" title="send_backward" /><p></td>
  <td style="text-align:center;">send_to_back<br><p><img src="svgs/Media & editing/send_to_back.svg" width="40" title="send_to_back" /><p></td>
  <td style="text-align:center;">sharpness<br><p><img src="svgs/Media & editing/sharpness.svg" width="40" title="sharpness" /><p></td>
  <td style="text-align:center;">shear<br><p><img src="svgs/Media & editing/shear.svg" width="40" title="shear" /><p></td>
  <td style="text-align:center;">shutter_outlined<br><p><img src="svgs/Media & editing/shutter_outlined.svg" width="40" title="shutter_outlined" /><p></td>
  <td style="text-align:center;">shutter<br><p><img src="svgs/Media & editing/shutter.svg" width="40" title="shutter" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">slideshow_outlined<br><p><img src="svgs/Media & editing/slideshow_outlined.svg" width="40" title="slideshow_outlined" /><p></td>
  <td style="text-align:center;">slideshow<br><p><img src="svgs/Media & editing/slideshow.svg" width="40" title="slideshow" /><p></td>
  <td style="text-align:center;">small_caps<br><p><img src="svgs/Media & editing/small_caps.svg" width="40" title="small_caps" /><p></td>
  <td style="text-align:center;">snap_to<br><p><img src="svgs/Media & editing/snap_to.svg" width="40" title="snap_to" /><p></td>
  <td style="text-align:center;">spiral<br><p><img src="svgs/Media & editing/spiral.svg" width="40" title="spiral" /><p></td>
  <td style="text-align:center;">stamp<br><p><img src="svgs/Media & editing/stamp.svg" width="40" title="stamp" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">strikethrough<br><p><img src="svgs/Media & editing/strikethrough.svg" width="40" title="strikethrough" /><p></td>
  <td style="text-align:center;">stroke_weight<br><p><img src="svgs/Media & editing/stroke_weight.svg" width="40" title="stroke_weight" /><p></td>
  <td style="text-align:center;">substract<br><p><img src="svgs/Media & editing/substract.svg" width="40" title="substract" /><p></td>
  <td style="text-align:center;">switch_camera<br><p><img src="svgs/Media & editing/switch_camera.svg" width="40" title="switch_camera" /><p></td>
  <td style="text-align:center;">text_center<br><p><img src="svgs/Media & editing/text_center.svg" width="40" title="text_center" /><p></td>
  <td style="text-align:center;">text_left<br><p><img src="svgs/Media & editing/text_left.svg" width="40" title="text_left" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">text_right<br><p><img src="svgs/Media & editing/text_right.svg" width="40" title="text_right" /><p></td>
  <td style="text-align:center;">text<br><p><img src="svgs/Media & editing/text.svg" width="40" title="text" /><p></td>
  <td style="text-align:center;">texture<br><p><img src="svgs/Media & editing/texture.svg" width="40" title="texture" /><p></td>
  <td style="text-align:center;">tools<br><p><img src="svgs/Media & editing/tools.svg" width="40" title="tools" /><p></td>
  <td style="text-align:center;">transform_artboard<br><p><img src="svgs/Media & editing/transform_artboard.svg" width="40" title="transform_artboard" /><p></td>
  <td style="text-align:center;">transform<br><p><img src="svgs/Media & editing/transform.svg" width="40" title="transform" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">underline<br><p><img src="svgs/Media & editing/underline.svg" width="40" title="underline" /><p></td>
  <td style="text-align:center;">undo<br><p><img src="svgs/Media & editing/undo.svg" width="40" title="undo" /><p></td>
  <td style="text-align:center;">union<br><p><img src="svgs/Media & editing/union.svg" width="40" title="union" /><p></td>
  <td style="text-align:center;">vector<br><p><img src="svgs/Media & editing/vector.svg" width="40" title="vector" /><p></td>
  <td style="text-align:center;">vertical_align_bottom<br><p><img src="svgs/Media & editing/vertical_align_bottom.svg" width="40" title="vertical_align_bottom" /><p></td>
  <td style="text-align:center;">vertical_align_middle<br><p><img src="svgs/Media & editing/vertical_align_middle.svg" width="40" title="vertical_align_middle" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">vertical_align_top<br><p><img src="svgs/Media & editing/vertical_align_top.svg" width="40" title="vertical_align_top" /><p></td>
  <td style="text-align:center;">video_add<br><p><img src="svgs/Media & editing/video_add.svg" width="40" title="video_add" /><p></td>
  <td style="text-align:center;">video_gallery_outlined<br><p><img src="svgs/Media & editing/video_gallery_outlined.svg" width="40" title="video_gallery_outlined" /><p></td>
  <td style="text-align:center;">video_gallery<br><p><img src="svgs/Media & editing/video_gallery.svg" width="40" title="video_gallery" /><p></td>
  <td style="text-align:center;">video_horizontal_outlined<br><p><img src="svgs/Media & editing/video_horizontal_outlined.svg" width="40" title="video_horizontal_outlined" /><p></td>
  <td style="text-align:center;">video_vertical_outlined<br><p><img src="svgs/Media & editing/video_vertical_outlined.svg" width="40" title="video_vertical_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">video_vertical<br><p><img src="svgs/Media & editing/video_vertical.svg" width="40" title="video_vertical" /><p></td>
</tr>
</table>

##### Other
<table>
<tr>
  <td style="text-align:center;">american_football<br><p><img src="svgs/Other/american_football.svg" width="40" title="american_football" /><p></td>
  <td style="text-align:center;">anchor<br><p><img src="svgs/Other/anchor.svg" width="40" title="anchor" /><p></td>
  <td style="text-align:center;">armchair<br><p><img src="svgs/Other/armchair.svg" width="40" title="armchair" /><p></td>
  <td style="text-align:center;">baseball<br><p><img src="svgs/Other/baseball.svg" width="40" title="baseball" /><p></td>
  <td style="text-align:center;">basketball<br><p><img src="svgs/Other/basketball.svg" width="40" title="basketball" /><p></td>
  <td style="text-align:center;">birthday_outlined<br><p><img src="svgs/Other/birthday_outlined.svg" width="40" title="birthday_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">birthday<br><p><img src="svgs/Other/birthday.svg" width="40" title="birthday" /><p></td>
  <td style="text-align:center;">block<br><p><img src="svgs/Other/block.svg" width="40" title="block" /><p></td>
  <td style="text-align:center;">book_bookmarked<br><p><img src="svgs/Other/book_bookmarked.svg" width="40" title="book_bookmarked" /><p></td>
  <td style="text-align:center;">book_opened<br><p><img src="svgs/Other/book_opened.svg" width="40" title="book_opened" /><p></td>
  <td style="text-align:center;">book_outlined<br><p><img src="svgs/Other/book_outlined.svg" width="40" title="book_outlined" /><p></td>
  <td style="text-align:center;">book<br><p><img src="svgs/Other/book.svg" width="40" title="book" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">brain<br><p><img src="svgs/Other/brain.svg" width="40" title="brain" /><p></td>
  <td style="text-align:center;">brick_wall<br><p><img src="svgs/Other/brick_wall.svg" width="40" title="brick_wall" /><p></td>
  <td style="text-align:center;">briefcase_not_accessible<br><p><img src="svgs/Other/briefcase_not_accessible.svg" width="40" title="briefcase_not_accessible" /><p></td>
  <td style="text-align:center;">briefcase_outlined<br><p><img src="svgs/Other/briefcase_outlined.svg" width="40" title="briefcase_outlined" /><p></td>
  <td style="text-align:center;">briefcase<br><p><img src="svgs/Other/briefcase.svg" width="40" title="briefcase" /><p></td>
  <td style="text-align:center;">chair<br><p><img src="svgs/Other/chair.svg" width="40" title="chair" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">circle_outlined<br><p><img src="svgs/Other/circle_outlined.svg" width="40" title="circle_outlined" /><p></td>
  <td style="text-align:center;">circle<br><p><img src="svgs/Other/circle.svg" width="40" title="circle" /><p></td>
  <td style="text-align:center;">cricket<br><p><img src="svgs/Other/cricket.svg" width="40" title="cricket" /><p></td>
  <td style="text-align:center;">crossfit<br><p><img src="svgs/Other/crossfit.svg" width="40" title="crossfit" /><p></td>
  <td style="text-align:center;">cube<br><p><img src="svgs/Other/cube.svg" width="40" title="cube" /><p></td>
  <td style="text-align:center;">dining_table_outlined<br><p><img src="svgs/Other/dining_table_outlined.svg" width="40" title="dining_table_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">dining_table<br><p><img src="svgs/Other/dining_table.svg" width="40" title="dining_table" /><p></td>
  <td style="text-align:center;">diving<br><p><img src="svgs/Other/diving.svg" width="40" title="diving" /><p></td>
  <td style="text-align:center;">door<br><p><img src="svgs/Other/door.svg" width="40" title="door" /><p></td>
  <td style="text-align:center;">crossfit<br><p><img src="svgs/Other/crossfit.svg" width="40" title="crossfit" /><p></td>
  <td style="text-align:center;">flag_cross_1<br><p><img src="svgs/Other/flag_cross_1.svg" width="40" title="flag_cross_1" /><p></td>
  <td style="text-align:center;">flag_cross_2<br><p><img src="svgs/Other/flag_cross_2.svg" width="40" title="flag_cross_2" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">flag_half_1<br><p><img src="svgs/Other/flag_half_1.svg" width="40" title="flag_half_1" /><p></td>
  <td style="text-align:center;">flag_half_2<br><p><img src="svgs/Other/flag_half_2.svg" width="40" title="flag_half_2" /><p></td>
  <td style="text-align:center;">flag_outlined<br><p><img src="svgs/Other/flag_outlined.svg" width="40" title="flag_outlined" /><p></td>
  <td style="text-align:center;">flag_quarter_1<br><p><img src="svgs/Other/flag_quarter_1.svg" width="40" title="flag_quarter_1" /><p></td>
  <td style="text-align:center;">flag_quarter_2<br><p><img src="svgs/Other/flag_quarter_2.svg" width="40" title="flag_quarter_2" /><p></td>
  <td style="text-align:center;">flag<br><p><img src="svgs/Other/flag.svg" width="40" title="flag" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">flask<br><p><img src="svgs/Other/flask.svg" width="40" title="flask" /><p></td>
  <td style="text-align:center;">flower<br><p><img src="svgs/Other/flower.svg" width="40" title="flower" /><p></td>
  <td style="text-align:center;">foot_print<br><p><img src="svgs/Other/foot_print.svg" width="40" title="foot_print" /><p></td>
  <td style="text-align:center;">gift<br><p><img src="svgs/Other/gift.svg" width="40" title="gift" /><p></td>
  <td style="text-align:center;">glasses_outlined<br><p><img src="svgs/Other/glasses_outlined.svg" width="40" title="glasses_outlined" /><p></td>
  <td style="text-align:center;">glasses<br><p><img src="svgs/Other/glasses.svg" width="40" title="glasses" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">hourglass_half<br><p><img src="svgs/Other/hourglass_half.svg" width="40" title="hourglass_half" /><p></td>
  <td style="text-align:center;">hourglass_outlined<br><p><img src="svgs/Other/hourglass_outlined.svg" width="40" title="hourglass_outlined" /><p></td>
  <td style="text-align:center;">hourglass_quarter<br><p><img src="svgs/Other/hourglass_quarter.svg" width="40" title="hourglass_quarter" /><p></td>
  <td style="text-align:center;">hourglass<br><p><img src="svgs/Other/hourglass.svg" width="40" title="hourglass" /><p></td>
  <td style="text-align:center;">ice_hockey<br><p><img src="svgs/Other/ice_hockey.svg" width="40" title="ice_hockey" /><p></td>
  <td style="text-align:center;">infinity<br><p><img src="svgs/Other/infinity.svg" width="40" title="infinity" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">justice<br><p><img src="svgs/Other/justice.svg" width="40" title="justice" /><p></td>
  <td style="text-align:center;">lab<br><p><img src="svgs/Other/lab.svg" width="40" title="lab" /><p></td>
  <td style="text-align:center;">leaf_diagonal<br><p><img src="svgs/Other/leaf_diagonal.svg" width="40" title="leaf_diagonal" /><p></td>
  <td style="text-align:center;">leaf<br><p><img src="svgs/Other/leaf.svg" width="40" title="leaf" /><p></td>
  <td style="text-align:center;">light_on<br><p><img src="svgs/Other/light_on.svg" width="40" title="light_on" /><p></td>
  <td style="text-align:center;">lightbulb<br><p><img src="svgs/Other/lightbulb.svg" width="40" title="lightbulb" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">medicaments<br><p><img src="svgs/Other/medicaments.svg" width="40" title="medicaments" /><p></td>
  <td style="text-align:center;">mma<br><p><img src="svgs/Other/mma.svg" width="40" title="mma" /><p></td>
  <td style="text-align:center;">moon_outlined<br><p><img src="svgs/Other/moon_outlined.svg" width="40" title="moon_outlined" /><p></td>
  <td style="text-align:center;">moon_vs_outlined<br><p><img src="svgs/Other/moon_vs_outlined.svg" width="40" title="moon_vs_outlined" /><p></td>
  <td style="text-align:center;">moon_vs<br><p><img src="svgs/Other/light_on.svg" width="40" title="light_on" /><p></td>
  <td style="text-align:center;">moon<br><p><img src="svgs/Other/lightbulb.svg" width="40" title="lightbulb" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">movie<br><p><img src="svgs/Other/movie.svg" width="40" title="movie" /><p></td>
  <td style="text-align:center;">node_multiple_outlined<br><p><img src="svgs/Other/node_multiple_outlined.svg" width="40" title="node_multiple_outlined" /><p></td>
  <td style="text-align:center;">node_multiple<br><p><img src="svgs/Other/node_multiple.svg" width="40" title="node_multiple" /><p></td>
  <td style="text-align:center;">node<br><p><img src="svgs/Other/node.svg" width="40" title="node" /><p></td>
  <td style="text-align:center;">pet<br><p><img src="svgs/Other/pet.svg" width="40" title="pet" /><p></td>
  <td style="text-align:center;">pill<br><p><img src="svgs/Other/pill.svg" width="40" title="pill" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">puzzle<br><p><img src="svgs/Other/puzzle.svg" width="40" title="puzzle" /><p></td>
  <td style="text-align:center;">quotes<br><p><img src="svgs/Other/quotes.svg" width="40" title="quotes" /><p></td>
  <td style="text-align:center;">recycling<br><p><img src="svgs/Other/recycling.svg" width="40" title="recycling" /><p></td>
  <td style="text-align:center;">rugby<br><p><img src="svgs/Other/rugby.svg" width="40" title="rugby" /><p></td>
  <td style="text-align:center;">seat_outlined<br><p><img src="svgs/Other/seat_outlined.svg" width="40" title="seat_outlined" /><p></td>
  <td style="text-align:center;">seat<br><p><img src="svgs/Other/seat.svg" width="40" title="seat" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">shoe_print_outlined<br><p><img src="svgs/Other/shoe_print_outlined.svg" width="40" title="shoe_print_outlined" /><p></td>
  <td style="text-align:center;">shoe_print<br><p><img src="svgs/Other/shoe_print.svg" width="40" title="shoe_print" /><p></td>
  <td style="text-align:center;">shower<br><p><img src="svgs/Other/shower.svg" width="40" title="shower" /><p></td>
  <td style="text-align:center;">sign_man<br><p><img src="svgs/Other/sign_man.svg" width="40" title="sign_man" /><p></td>
  <td style="text-align:center;">sign_woman<br><p><img src="svgs/Other/sign_woman.svg" width="40" title="sign_woman" /><p></td>
  <td style="text-align:center;">skull<br><p><img src="svgs/Other/skull.svg" width="40" title="skull" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">smoke_free<br><p><img src="svgs/Other/smoke_free.svg" width="40" title="smoke_free" /><p></td>
  <td style="text-align:center;">smoking<br><p><img src="svgs/Other/smoking.svg" width="40" title="smoking" /><p></td>
  <td style="text-align:center;">snowflake<br><p><img src="svgs/Other/snowflake.svg" width="40" title="snowflake" /><p></td>
  <td style="text-align:center;">sport<br><p><img src="svgs/Other/sport.svg" width="40" title="sport" /><p></td>
  <td style="text-align:center;">square_outlined<br><p><img src="svgs/Other/square_outlined.svg" width="40" title="square_outlined" /><p></td>
  <td style="text-align:center;">square<br><p><img src="svgs/Other/square.svg" width="40" title="square" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">stairs_down<br><p><img src="svgs/Other/stairs_down.svg" width="40" title="stairs_down" /><p></td>
  <td style="text-align:center;">stairs_up<br><p><img src="svgs/Other/stairs_up.svg" width="40" title="stairs_up" /><p></td>
  <td style="text-align:center;">survey<br><p><img src="svgs/Other/survey.svg" width="40" title="survey" /><p></td>
  <td style="text-align:center;">t_shirt<br><p><img src="svgs/Other/t_shirt.svg" width="40" title="t_shirt" /><p></td>
  <td style="text-align:center;">table_tennis<br><p><img src="svgs/Other/table_tennis.svg" width="40" title="table_tennis" /><p></td>
  <td style="text-align:center;">temperature<br><p><img src="svgs/Other/temperature.svg" width="40" title="temperature" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">tennis<br><p><img src="svgs/Other/tennis.svg" width="40" title="tennis" /><p></td>
  <td style="text-align:center;">test-tube<br><p><img src="svgs/Other/test-tube.svg" width="40" title="test-tube" /><p></td>
  <td style="text-align:center;">ticket<br><p><img src="svgs/Other/ticket.svg" width="40" title="ticket" /><p></td>
  <td style="text-align:center;">triangle_outlined<br><p><img src="svgs/Other/triangle_outlined.svg" width="40" title="triangle_outlined" /><p></td>
  <td style="text-align:center;">triangle<br><p><img src="svgs/Other/triangle.svg" width="40" title="triangle" /><p></td>
  <td style="text-align:center;">umbrella_outlined<br><p><img src="svgs/Other/umbrella_outlined.svg" width="40" title="umbrella_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">volleyball<br><p><img src="svgs/Other/volleyball.svg" width="40" title="volleyball" /><p></td>
  <td style="text-align:center;">weight_outlined<br><p><img src="svgs/Other/weight_outlined.svg" width="40" title="weight_outlined" /><p></td>
  <td style="text-align:center;">weight<br><p><img src="svgs/Other/weight.svg" width="40" title="weight" /><p></td>
  <td style="text-align:center;">wishlist_outlined<br><p><img src="svgs/Other/wishlist_outlined.svg" width="40" title="wishlist_outlined" /><p></td>
  <td style="text-align:center;">wishlist<br><p><img src="svgs/Other/wishlist.svg" width="40" title="wishlist" /><p></td>
  <td style="text-align:center;">wrench<br><p><img src="svgs/Other/wrench.svg" width="40" title="wrench" /><p></td>
</tr>
</table>

##### Payment

##### People and activity
<table>
<tr>
  <td style="text-align:center;">accessibility_vs<br><p><img src="svgs/People & activity/accessibility_vs.svg" width="40" title="accessibility_vs" /><p></td>
  <td style="text-align:center;">accessibility<br><p><img src="svgs/People & activity/accessibility.svg" width="40" title="accessibility" /><p></td>
  <td style="text-align:center;">accessible_forward<br><p><img src="svgs/People & activity/accessible_forward.svg" width="40" title="accessible_forward" /><p></td>
  <td style="text-align:center;">accessible<br><p><img src="svgs/People & activity/accessible.svg" width="40" title="accessible" /><p></td>
  <td style="text-align:center;">hiking<br><p><img src="svgs/People & activity/hiking.svg" width="40" title="hiking" /><p></td>
  <td style="text-align:center;">man<br><p><img src="svgs/People & activity/man.svg" width="40" title="man" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">pregnancy<br><p><img src="svgs/People & activity/pregnancy.svg" width="40" title="pregnancy" /><p></td>
  <td style="text-align:center;">running<br><p><img src="svgs/People & activity/running.svg" width="40" title="running" /><p></td>
  <td style="text-align:center;">standing<br><p><img src="svgs/People & activity/standing.svg" width="40" title="standing" /><p></td>
  <td style="text-align:center;">swimming<br><p><img src="svgs/People & activity/swimming.svg" width="40" title="swimming" /><p></td>
  <td style="text-align:center;">walking<br><p><img src="svgs/People & activity/walking.svg" width="40" title="walking" /><p></td>
  <td style="text-align:center;">woman<br><p><img src="svgs/People & activity/woman.svg" width="40" title="woman" /><p></td>
</tr>
</table>

##### Premium
<table>
<tr>
  <td style="text-align:center;">award_outlined<br><p><img src="svgs/Premium/award_outlined.svg" width="40" title="award_outlined" /><p></td>
  <td style="text-align:center;">award<br><p><img src="svgs/Premium/award.svg" width="40" title="award" /><p></td>
  <td style="text-align:center;">boost<br><p><img src="svgs/Premium/boost.svg" width="40" title="boost" /><p></td>
  <td style="text-align:center;">crown_outlined<br><p><img src="svgs/Premium/crown_outlined.svg" width="40" title="crown_outlined" /><p></td>
  <td style="text-align:center;">crown<br><p><img src="svgs/Premium/crown.svg" width="40" title="crown" /><p></td>
  <td style="text-align:center;">diamond_outlined<br><p><img src="svgs/Premium/diamond_outlined.svg" width="40" title="diamond_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">diamond<br><p><img src="svgs/Premium/diamond.svg" width="40" title="diamond" /><p></td>
  <td style="text-align:center;">dna_spiral<br><p><img src="svgs/Premium/dna_spiral.svg" width="40" title="dna_spiral" /><p></td>
  <td style="text-align:center;">luck<br><p><img src="svgs/Premium/luck.svg" width="40" title="luck" /><p></td>
  <td style="text-align:center;">medal<br><p><img src="svgs/Premium/medal.svg" width="40" title="medal" /><p></td>
  <td style="text-align:center;">planet<br><p><img src="svgs/Premium/planet.svg" width="40" title="planet" /><p></td>
  <td style="text-align:center;">premium_outlined<br><p><img src="svgs/Premium/premium_outlined.svg" width="40" title="premium_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">robot<br><p><img src="svgs/Premium/robot.svg" width="40" title="robot" /><p></td>
  <td style="text-align:center;">sticker_outlined<br><p><img src="svgs/Premium/sticker_outlined.svg" width="40" title="sticker_outlined" /><p></td>
  <td style="text-align:center;">sticker<br><p><img src="svgs/Premium/sticker.svg" width="40" title="sticker" /><p></td>
  <td style="text-align:center;">top_security_outlined<br><p><img src="svgs/Premium/top_security_outlined.svg" width="40" title="top_security_outlined" /><p></td>
  <td style="text-align:center;">top_security<br><p><img src="svgs/Premium/top_security.svg" width="40" title="top_security" /><p></td>
  <td style="text-align:center;">verified_outlined<br><p><img src="svgs/Premium/verified_outlined.svg" width="40" title="verified_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">verified<br><p><img src="svgs/Premium/verified.svg" width="40" title="verified" /><p></td>
</tr>
</table>

##### Social media and tools
<table>
<tr>
  <td style="text-align:center;">appstore<br><p><img src="svgs/Social media & tools/appstore.svg" width="40" title="appstore" /><p></td>
  <td style="text-align:center;">behance<br><p><img src="svgs/Social media & tools/behance.svg" width="40" title="behance" /><p></td>
  <td style="text-align:center;">dribbble<br><p><img src="svgs/Social media & tools/dribbble.svg" width="40" title="dribbble" /><p></td>
  <td style="text-align:center;">dropbox<br><p><img src="svgs/Social media & tools/dropbox.svg" width="40" title="dropbox" /><p></td>
  <td style="text-align:center;">facebook_square<br><p><img src="svgs/Social media & tools/facebook_square.svg" width="40" title="facebook_square" /><p></td>
  <td style="text-align:center;">facebook<br><p><img src="svgs/Social media & tools/facebook.svg" width="40" title="facebook" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">github<br><p><img src="svgs/Social media & tools/github.svg" width="40" title="github" /><p></td>
  <td style="text-align:center;">gmail_outlined<br><p><img src="svgs/Social media & tools/gmail_outlined.svg" width="40" title="gmail_outlined" /><p></td>
  <td style="text-align:center;">gmail<br><p><img src="svgs/Social media & tools/gmail.svg" width="40" title="gmail" /><p></td>
  <td style="text-align:center;">google_drive<br><p><img src="svgs/Social media & tools/google_drive.svg" width="40" title="google_drive" /><p></td>
  <td style="text-align:center;">google_play<br><p><img src="svgs/Social media & tools/google_play.svg" width="40" title="google_play" /><p></td>
  <td style="text-align:center;">google<br><p><img src="svgs/Social media & tools/google.svg" width="40" title="google" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">hangouts_outlined<br><p><img src="svgs/Social media & tools/hangouts_outlined.svg" width="40" title="hangouts_outlined" /><p></td>
  <td style="text-align:center;">instagram<br><p><img src="svgs/Social media & tools/instagram.svg" width="40" title="instagram" /><p></td>
  <td style="text-align:center;">linkedin_square<br><p><img src="svgs/Social media & tools/linkedin_square.svg" width="40" title="linkedin_square" /><p></td>
  <td style="text-align:center;">linkedin<br><p><img src="svgs/Social media & tools/linkedin.svg" width="40" title="linkedin" /><p></td>
  <td style="text-align:center;">medium_square<br><p><img src="svgs/Social media & tools/medium_square.svg" width="40" title="medium_square" /><p></td>
  <td style="text-align:center;">medium<br><p><img src="svgs/Social media & tools/medium.svg" width="40" title="medium" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">messenger_outlined<br><p><img src="svgs/Social media & tools/messenger_outlined.svg" width="40" title="messenger_outlined" /><p></td>
  <td style="text-align:center;">messenger<br><p><img src="svgs/Social media & tools/messenger.svg" width="40" title="messenger" /><p></td>
  <td style="text-align:center;">pinterest_circle<br><p><img src="svgs/Social media & tools/pinterest_circle.svg" width="40" title="pinterest_circle" /><p></td>
  <td style="text-align:center;">pinterest<br><p><img src="svgs/Social media & tools/pinterest.svg" width="40" title="pinterest" /><p></td>
  <td style="text-align:center;">reddit<br><p><img src="svgs/Social media & tools/reddit.svg" width="40" title="reddit" /><p></td>
  <td style="text-align:center;">rss<br><p><img src="svgs/Social media & tools/rss.svg" width="40" title="rss" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">skype<br><p><img src="svgs/Social media & tools/skype.svg" width="40" title="skype" /><p></td>
  <td style="text-align:center;">slack<br><p><img src="svgs/Social media & tools/slack.svg" width="40" title="slack" /><p></td>
  <td style="text-align:center;">snapchat_outlined<br><p><img src="svgs/Social media & tools/snapchat_outlined.svg" width="40" title="snapchat_outlined" /><p></td>
  <td style="text-align:center;">snapchat<br><p><img src="svgs/Social media & tools/snapchat.svg" width="40" title="snapchat" /><p></td>
  <td style="text-align:center;">steam<br><p><img src="svgs/Social media & tools/steam.svg" width="40" title="steam" /><p></td>
  <td style="text-align:center;">telegram<br><p><img src="svgs/Social media & tools/telegram.svg" width="40" title="telegram" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">tik_tok<br><p><img src="svgs/Social media & tools/tik_tok.svg" width="40" title="tik_tok" /><p></td>
  <td style="text-align:center;">twitch<br><p><img src="svgs/Social media & tools/twitch.svg" width="40" title="twitch" /><p></td>
  <td style="text-align:center;">twitter<br><p><img src="svgs/Social media & tools/twitter.svg" width="40" title="twitter" /><p></td>
  <td style="text-align:center;">viber_outlined<br><p><img src="svgs/Social media & tools/viber_outlined.svg" width="40" title="viber_outlined" /><p></td>
  <td style="text-align:center;">viber<br><p><img src="svgs/Social media & tools/viber.svg" width="40" title="viber" /><p></td>
  <td style="text-align:center;">vimeo_square<br><p><img src="svgs/Social media & tools/vimeo_square.svg" width="40" title="vimeo_square" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">vimeo<br><p><img src="svgs/Social media & tools/vimeo.svg" width="40" title="vimeo" /><p></td>
  <td style="text-align:center;">vkontakte<br><p><img src="svgs/Social media & tools/vkontakte.svg" width="40" title="vkontakte" /><p></td>
  <td style="text-align:center;">whatsapp_outlined<br><p><img src="svgs/Social media & tools/whatsapp_outlined.svg" width="40" title="whatsapp_outlined" /><p></td>
  <td style="text-align:center;">whatsapp<br><p><img src="svgs/Social media & tools/whatsapp.svg" width="40" title="whatsapp" /><p></td>
  <td style="text-align:center;">youtube_outlined<br><p><img src="svgs/Social media & tools/youtube_outlined.svg" width="40" title="youtube_outlined" /><p></td>
  <td style="text-align:center;">youtube<br><p><img src="svgs/Social media & tools/youtube.svg" width="40" title="youtube" /><p></td>
</tr>
</table>

##### Technology and data
<table>
<tr>
  <td style="text-align:center;">airdrop<br><p><img src="svgs/Technology & data/airdrop.svg" width="40" title="airdrop" /><p></td>
  <td style="text-align:center;">airplay<br><p><img src="svgs/Technology & data/airplay.svg" width="40" title="airplay" /><p></td>
  <td style="text-align:center;">android<br><p><img src="svgs/Technology & data/android.svg" width="40" title="android" /><p></td>
  <td style="text-align:center;">apple_outlined<br><p><img src="svgs/Technology & data/apple_outlined.svg" width="40" title="apple_outlined" /><p></td>
  <td style="text-align:center;">apple<br><p><img src="svgs/Technology & data/apple.svg" width="40" title="apple" /><p></td>
  <td style="text-align:center;">augmented_reality<br><p><img src="svgs/Technology & data/augmented_reality.svg" width="40" title="augmented_reality" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">barcode<br><p><img src="svgs/Technology & data/barcode.svg" width="40" title="barcode" /><p></td>
  <td style="text-align:center;">bluetooth_connect<br><p><img src="svgs/Technology & data/bluetooth_connect.svg" width="40" title="bluetooth_connect" /><p></td>
  <td style="text-align:center;">bluetooth_off<br><p><img src="svgs/Technology & data/bluetooth_off.svg" width="40" title="bluetooth_off" /><p></td>
  <td style="text-align:center;">bluetooth_search<br><p><img src="svgs/Technology & data/bluetooth_search.svg" width="40" title="bluetooth_search" /><p></td>
  <td style="text-align:center;">bluetooth<br><p><img src="svgs/Technology & data/bluetooth.svg" width="40" title="bluetooth" /><p></td>
  <td style="text-align:center;">chart_bar_1<br><p><img src="svgs/Technology & data/chart_bar_1.svg" width="40" title="chart_bar_1" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">chart_bar_2<br><p><img src="svgs/Technology & data/chart_bar_2.svg" width="40" title="chart_bar_2" /><p></td>
  <td style="text-align:center;">chart_bar_3<br><p><img src="svgs/Technology & data/chart_bar_3.svg" width="40" title="chart_bar_3" /><p></td>
  <td style="text-align:center;">chart_bar_4<br><p><img src="svgs/Technology & data/chart_bar_4.svg" width="40" title="chart_bar_4" /><p></td>
  <td style="text-align:center;">chart_bubble<br><p><img src="svgs/Technology & data/chart_bubble.svg" width="40" title="chart_bubble" /><p></td>
  <td style="text-align:center;">chart_donut_1<br><p><img src="svgs/Technology & data/chart_donut_1.svg" width="40" title="chart_donut_1" /><p></td>
  <td style="text-align:center;">chart_donut_2<br><p><img src="svgs/Technology & data/chart_donut_2.svg" width="40" title="chart_donut_2" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">chart_line_down<br><p><img src="svgs/Technology & data/chart_line_down.svg" width="40" title="chart_line_down" /><p></td>
  <td style="text-align:center;">chart_line_up<br><p><img src="svgs/Technology & data/chart_line_up.svg" width="40" title="chart_line_up" /><p></td>
  <td style="text-align:center;">chart_pie_1<br><p><img src="svgs/Technology & data/chart_pie_1.svg" width="40" title="chart_pie_1" /><p></td>
  <td style="text-align:center;">chart_pie_2<br><p><img src="svgs/Technology & data/chart_pie_2.svg" width="40" title="chart_pie_2" /><p></td>
  <td style="text-align:center;">cloud_disabled<br><p><img src="svgs/Technology & data/cloud_disabled.svg" width="40" title="cloud_disabled" /><p></td>
  <td style="text-align:center;">cloud_off_outlined<br><p><img src="svgs/Technology & data/cloud_off_outlined.svg" width="40" title="cloud_off_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">cloud_off<br><p><img src="svgs/Technology & data/cloud_off.svg" width="40" title="cloud_off" /><p></td>
  <td style="text-align:center;">cloud_on<br><p><img src="svgs/Technology & data/cloud_on.svg" width="40" title="cloud_on" /><p></td>
  <td style="text-align:center;">cloud_outlined<br><p><img src="svgs/Technology & data/cloud_outlined.svg" width="40" title="cloud_outlined" /><p></td>
  <td style="text-align:center;">cloud<br><p><img src="svgs/Technology & data/cloud.svg" width="40" title="cloud" /><p></td>
  <td style="text-align:center;">code<br><p><img src="svgs/Technology & data/code.svg" width="40" title="code" /><p></td>
  <td style="text-align:center;">data_sharing<br><p><img src="svgs/Technology & data/data_sharing.svg" width="40" title="data_sharing" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">download_from_cloud<br><p><img src="svgs/Technology & data/download_from_cloud.svg" width="40" title="download_from_cloud" /><p></td>
  <td style="text-align:center;">face_id<br><p><img src="svgs/Technology & data/face_id.svg" width="40" title="face_id" /><p></td>
  <td style="text-align:center;">fingerprint<br><p><img src="svgs/Technology & data/fingerprint.svg" width="40" title="fingerprint" /><p></td>
  <td style="text-align:center;">link_off<br><p><img src="svgs/Technology & data/link_off.svg" width="40" title="link_off" /><p></td>
  <td style="text-align:center;">link<br><p><img src="svgs/Technology & data/link.svg" width="40" title="link" /><p></td>
  <td style="text-align:center;">memory_chip<br><p><img src="svgs/Technology & data/memory_chip.svg" width="40" title="memory_chip" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">mobile_data_low<br><p><img src="svgs/Technology & data/mobile_data_low.svg" width="40" title="mobile_data_low" /><p></td>
  <td style="text-align:center;">mobile_data<br><p><img src="svgs/Technology & data/mobile_data.svg" width="40" title="mobile_data" /><p></td>
  <td style="text-align:center;">online<br><p><img src="svgs/Technology & data/online.svg" width="40" title="online" /><p></td>
  <td style="text-align:center;">plugin_outlined<br><p><img src="svgs/Technology & data/plugin_outlined.svg" width="40" title="plugin_outlined" /><p></td>
  <td style="text-align:center;">plugin<br><p><img src="svgs/Technology & data/plugin.svg" width="40" title="plugin" /><p></td>
  <td style="text-align:center;">poll<br><p><img src="svgs/Technology & data/poll.svg" width="40" title="poll" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">qr_code<br><p><img src="svgs/Technology & data/qr_code.svg" width="40" title="qr_code" /><p></td>
  <td style="text-align:center;">scan<br><p><img src="svgs/Technology & data/scan.svg" width="40" title="scan" /><p></td>
  <td style="text-align:center;">security_off_outlined<br><p><img src="svgs/Technology & data/security_off_outlined.svg" width="40" title="security_off_outlined" /><p></td>
  <td style="text-align:center;">security_off<br><p><img src="svgs/Technology & data/security_off.svg" width="40" title="security_off" /><p></td>
  <td style="text-align:center;">security_on_outlined<br><p><img src="svgs/Technology & data/security_on_outlined.svg" width="40" title="security_on_outlined" /><p></td>
  <td style="text-align:center;">security_on<br><p><img src="svgs/Technology & data/security_on.svg" width="40" title="security_on" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">security<br><p><img src="svgs/Technology & data/security.svg" width="40" title="security" /><p></td>
  <td style="text-align:center;">touch_id<br><p><img src="svgs/Technology & data/touch_id.svg" width="40" title="touch_id" /><p></td>
  <td style="text-align:center;">trending_down<br><p><img src="svgs/Technology & data/trending_down.svg" width="40" title="trending_down" /><p></td>
  <td style="text-align:center;">trending_up<br><p><img src="svgs/Technology & data/trending_up.svg" width="40" title="trending_up" /><p></td>
  <td style="text-align:center;">upload_on_cloud<br><p><img src="svgs/Technology & data/upload_on_cloud.svg" width="40" title="upload_on_cloud" /><p></td>
  <td style="text-align:center;">usb<br><p><img src="svgs/Technology & data/usb.svg" width="40" title="usb" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">virtual_reality_outlined<br><p><img src="svgs/Technology & data/virtual_reality_outlined.svg" width="40" title="virtual_reality_outlined" /><p></td>
  <td style="text-align:center;">virtual_reality<br><p><img src="svgs/Technology & data/virtual_reality.svg" width="40" title="virtual_reality" /><p></td>
  <td style="text-align:center;">website<br><p><img src="svgs/Technology & data/website.svg" width="40" title="website" /><p></td>
  <td style="text-align:center;">wifi_off<br><p><img src="svgs/Technology & data/wifi_off.svg" width="40" title="wifi_off" /><p></td>
  <td style="text-align:center;">wifi<br><p><img src="svgs/Technology & data/wifi.svg" width="40" title="wifi" /><p></td>
  <td style="text-align:center;">windows<br><p><img src="svgs/Technology & data/windows.svg" width="40" title="windows" /><p></td>
</tr>
</table>

##### Transport
<table>
<tr>
  <td style="text-align:center;">arrival<br><p><img src="svgs/Transport/arrival.svg" width="40" title="arrival" /><p></td>
  <td style="text-align:center;">bike<br><p><img src="svgs/Transport/bike.svg" width="40" title="bike" /><p></td>
  <td style="text-align:center;">boat<br><p><img src="svgs/Transport/boat.svg" width="40" title="boat" /><p></td>
  <td style="text-align:center;">bus<br><p><img src="svgs/Transport/bus.svg" width="40" title="bus" /><p></td>
  <td style="text-align:center;">car_police<br><p><img src="svgs/Transport/car_police.svg" width="40" title="car_police" /><p></td>
  <td style="text-align:center;">car<br><p><img src="svgs/Transport/car.svg" width="40" title="car" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">departure<br><p><img src="svgs/Transport/departure.svg" width="40" title="departure" /><p></td>
  <td style="text-align:center;">flight_cancelled<br><p><img src="svgs/Transport/flight_cancelled.svg" width="40" title="flight_cancelled" /><p></td>
  <td style="text-align:center;">flight<br><p><img src="svgs/Transport/flight.svg" width="40" title="flight" /><p></td>
  <td style="text-align:center;">metro<br><p><img src="svgs/Transport/metro.svg" width="40" title="metro" /><p></td>
  <td style="text-align:center;">motocycle<br><p><img src="svgs/Transport/motocycle.svg" width="40" title="motocycle" /><p></td>
  <td style="text-align:center;">taxi<br><p><img src="svgs/Transport/taxi.svg" width="40" title="taxi" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">traffic_light<br><p><img src="svgs/Transport/traffic_light.svg" width="40" title="traffic_light" /><p></td>
  <td style="text-align:center;">train<br><p><img src="svgs/Transport/train.svg" width="40" title="train" /><p></td>
  <td style="text-align:center;">tram<br><p><img src="svgs/Transport/tram.svg" width="40" title="tram" /><p></td>
  <td style="text-align:center;">truck<br><p><img src="svgs/Transport/truck.svg" width="40" title="truck" /><p></td>
</tr>
</table>

##### UI
<table>
<tr>
  <td style="text-align:center;">add_event<br><p><img src="svgs/UI/add_event.svg" width="40" title="add_event" /><p></td>
  <td style="text-align:center;">add_to_trash<br><p><img src="svgs/UI/add_to_trash.svg" width="40" title="add_to_trash" /><p></td>
  <td style="text-align:center;">agenda_view_outlined<br><p><img src="svgs/UI/agenda_view_outlined.svg" width="40" title="agenda_view_outlined" /><p></td>
  <td style="text-align:center;">agenda_view<br><p><img src="svgs/UI/agenda_view.svg" width="40" title="agenda_view" /><p></td>
  <td style="text-align:center;">alarm_add<br><p><img src="svgs/UI/alarm_add.svg" width="40" title="alarm_add" /><p></td>
  <td style="text-align:center;">alarm_alert<br><p><img src="svgs/UI/alarm_alert.svg" width="40" title="alarm_alert" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">alarm_off<br><p><img src="svgs/UI/alarm_off.svg" width="40" title="alarm_off" /><p></td>
  <td style="text-align:center;">alarm_on<br><p><img src="svgs/UI/alarm_on.svg" width="40" title="alarm_on" /><p></td>
  <td style="text-align:center;">alarm<br><p><img src="svgs/UI/alarm.svg" width="40" title="alarm" /><p></td>
  <td style="text-align:center;">album<br><p><img src="svgs/UI/album.svg" width="40" title="album" /><p></td>
  <td style="text-align:center;">albums<br><p><img src="svgs/UI/albums.svg" width="40" title="albums" /><p></td>
  <td style="text-align:center;">all_done<br><p><img src="svgs/UI/all_done.svg" width="40" title="all_done" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">apps<br><p><img src="svgs/UI/apps.svg" width="40" title="apps" /><p></td>
  <td style="text-align:center;">uattachment_diagonalsers_switch<br><p><img src="svgs/UI/attachment_diagonal.svg" width="40" title="attachment_diagonal" /><p></td>
  <td style="text-align:center;">attachment<br><p><img src="svgs/UI/attachment.svg" width="40" title="attachment" /><p></td>
  <td style="text-align:center;">blocked<br><p><img src="svgs/UI/blocked.svg" width="40" title="blocked" /><p></td>
  <td style="text-align:center;">category_outlined<br><p><img src="svgs/UI/category_outlined.svg" width="40" title="category_outlined" /><p></td>
  <td style="text-align:center;">category<br><p><img src="svgs/UI/category.svg" width="40" title="category" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">dashboard_outlined<br><p><img src="svgs/UI/dashboard_outlined.svg" width="40" title="dashboard_outlined" /><p></td>
  <td style="text-align:center;">dashboard_vs_outlined<br><p><img src="svgs/UI/dashboard_vs_outlined.svg" width="40" title="dashboard_vs_outlined" /><p></td>
  <td style="text-align:center;">dashboard_vs<br><p><img src="svgs/UI/dashboard_vs.svg" width="40" title="dashboard_vs" /><p></td>
  <td style="text-align:center;">delete_outlined<br><p><img src="svgs/UI/delete_outlined.svg" width="40" title="delete_outlined" /><p></td>
  <td style="text-align:center;">download_outlined<br><p><img src="svgs/UI/download_outlined.svg" width="40" title="download_outlined" /><p></td>
  <td style="text-align:center;">download_to<br><p><img src="svgs/UI/download_to.svg" width="40" title="download_to" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">download<br><p><img src="svgs/UI/download.svg" width="40" title="download" /><p></td>
  <td style="text-align:center;">edit<br><p><img src="svgs/UI/edit.svg" width="40" title="edit" /><p></td>
  <td style="text-align:center;">explore_off_outlined<br><p><img src="svgs/UI/explore_off_outlined.svg" width="40" title="explore_off_outlined" /><p></td>
  <td style="text-align:center;">explore_off<br><p><img src="svgs/UI/explore_off.svg" width="40" title="explore_off" /><p></td>
  <td style="text-align:center;">explore_outlined<br><p><img src="svgs/UI/explore_outlined.svg" width="40" title="explore_outlined" /><p></td>
  <td style="text-align:center;">explore<br><p><img src="svgs/UI/explore.svg" width="40" title="explore" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">feed_outlined<br><p><img src="svgs/UI/feed_outlined.svg" width="40" title="feed_outlined" /><p></td>
  <td style="text-align:center;">feed<br><p><img src="svgs/UI/feed.svg" width="40" title="feed" /><p></td>
  <td style="text-align:center;">filter_list<br><p><img src="svgs/UI/filter_list.svg" width="40" title="filter_list" /><p></td>
  <td style="text-align:center;">filter_outlined<br><p><img src="svgs/UI/filter_outlined.svg" width="40" title="filter_outlined" /><p></td>
  <td style="text-align:center;">format_bullets<br><p><img src="svgs/UI/format_bullets.svg" width="40" title="format_bullets" /><p></td>
  <td style="text-align:center;">format_points<br><p><img src="svgs/UI/format_points.svg" width="40" title="format_points" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">hidden_outlined<br><p><img src="svgs/UI/hidden_outlined.svg" width="40" title="hidden_outlined" /><p></td>
  <td style="text-align:center;">hidden<br><p><img src="svgs/UI/hidden.svg" width="40" title="hidden" /><p></td>
  <td style="text-align:center;">home_outlined<br><p><img src="svgs/UI/home_outlined.svg" width="40" title="home_outlined" /><p></td>
  <td style="text-align:center;">home_vs_1_outlined<br><p><img src="svgs/UI/home_vs_1_outlined.svg" width="40" title="home_vs_1_outlined" /><p></td>
  <td style="text-align:center;">home_vs_2_outlined<br><p><img src="svgs/UI/home_vs_2_outlined.svg" width="40" title="home_vs_2_outlined" /><p></td>
  <td style="text-align:center;">home_vs<br><p><img src="svgs/UI/home_vs.svg" width="40" title="home_vs" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">home<br><p><img src="svgs/UI/home.svg" width="40" title="home" /><p></td>
  <td style="text-align:center;">hot<br><p><img src="svgs/UI/hot.svg" width="40" title="hot" /><p></td>
  <td style="text-align:center;">import<br><p><img src="svgs/UI/import.svg" width="40" title="import" /><p></td>
  <td style="text-align:center;">invisible<br><p><img src="svgs/UI/invisible.svg" width="40" title="invisible" /><p></td>
  <td style="text-align:center;">launch_outlined<br><p><img src="svgs/UI/launch_outlined.svg" width="40" title="launch_outlined" /><p></td>
  <td style="text-align:center;">launch_vs_outlined<br><p><img src="svgs/UI/launch_vs_outlined.svg" width="40" title="launch_vs_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">launch_vs<br><p><img src="svgs/UI/launch_vs.svg" width="40" title="launch_vs" /><p></td>
  <td style="text-align:center;">launch<br><p><img src="svgs/UI/launch.svg" width="40" title="launch" /><p></td>
  <td style="text-align:center;">lock_open_outlined<br><p><img src="svgs/UI/lock_open_outlined.svg" width="40" title="lock_open_outlined" /><p></td>
  <td style="text-align:center;">lock_opened<br><p><img src="svgs/UI/lock_opened.svg" width="40" title="lock_opened" /><p></td>
  <td style="text-align:center;">lock_outlined<br><p><img src="svgs/UI/lock_outlined.svg" width="40" title="lock_outlined" /><p></td>
  <td style="text-align:center;">lock<br><p><img src="svgs/UI/lock.svg" width="40" title="lock" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">menu_hamburger<br><p><img src="svgs/UI/menu_hamburger.svg" width="40" title="menu_hamburger" /><p></td>
  <td style="text-align:center;">menu_left_right<br><p><img src="svgs/UI/menu_left_right.svg" width="40" title="menu_left_right" /><p></td>
  <td style="text-align:center;">menu_left<br><p><img src="svgs/UI/menu_left.svg" width="40" title="menu_left" /><p></td>
  <td style="text-align:center;">menu_vs_outlined<br><p><img src="svgs/UI/menu_vs_outlined.svg" width="40" title="menu_vs_outlined" /><p></td>
  <td style="text-align:center;">menu_vs<br><p><img src="svgs/UI/menu_vs.svg" width="40" title="menu_vs" /><p></td>
  <td style="text-align:center;">more_horizontal<br><p><img src="svgs/UI/more_horizontal.svg" width="40" title="more_horizontal" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">more_vertical<br><p><img src="svgs/UI/more_vertical.svg" width="40" title="more_vertical" /><p></td>
  <td style="text-align:center;">new_message<br><p><img src="svgs/UI/new_message.svg" width="40" title="new_message" /><p></td>
  <td style="text-align:center;">password_open<br><p><img src="svgs/UI/password_open.svg" width="40" title="password_open" /><p></td>
  <td style="text-align:center;">password<br><p><img src="svgs/UI/password.svg" width="40" title="password" /><p></td>
  <td style="text-align:center;">remove_from_trash<br><p><img src="svgs/UI/remove_from_trash.svg" width="40" title="remove_from_trash" /><p></td>
  <td style="text-align:center;">settings_outlined<br><p><img src="svgs/UI/settings_outlined.svg" width="40" title="settings_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">settings_vs_outlined<br><p><img src="svgs/UI/settings_vs_outlined.svg" width="40" title="settings_vs_outlined" /><p></td>
  <td style="text-align:center;">settings_vs<br><p><img src="svgs/UI/settings_vs.svg" width="40" title="settings_vs" /><p></td>
  <td style="text-align:center;">share<br><p><img src="svgs/UI/share.svg" width="40" title="share" /><p></td>
  <td style="text-align:center;">sign_in<br><p><img src="svgs/UI/sign_in.svg" width="40" title="sign_in" /><p></td>
  <td style="text-align:center;">sign_out<br><p><img src="svgs/UI/sign_out.svg" width="40" title="sign_out" /><p></td>
  <td style="text-align:center;">support_vs<br><p><img src="svgs/UI/support_vs.svg" width="40" title="support_vs" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">table<br><p><img src="svgs/UI/table.svg" width="40" title="table" /><p></td>
  <td style="text-align:center;">toggle_off<br><p><img src="svgs/UI/toggle_off.svg" width="40" title="toggle_off" /><p></td>
  <td style="text-align:center;">toggle_on<br><p><img src="svgs/UI/toggle_on.svg" width="40" title="toggle_on" /><p></td>
  <td style="text-align:center;">upload_outlined<br><p><img src="svgs/UI/upload_outlined.svg" width="40" title="upload_outlined" /><p></td>
  <td style="text-align:center;">upload<br><p><img src="svgs/UI/upload.svg" width="40" title="upload" /><p></td>
  <td style="text-align:center;">visible_outlined<br><p><img src="svgs/UI/visible_outlined.svg" width="40" title="visible_outlined" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">visible<br><p><img src="svgs/UI/visible.svg" width="40" title="visible" /><p></td>
</tr>
</table>

##### User
<table>
<tr>
  <td style="text-align:center;">account_circle<br><p><img src="svgs/User/account_circle.svg" width="40" title="account_circle" /><p></td>
  <td style="text-align:center;">account_square_outlined<br><p><img src="svgs/User/account_square_outlined.svg" width="40" title="account_square_outlined" /><p></td>
  <td style="text-align:center;">account_square<br><p><img src="svgs/User/account_square.svg" width="40" title="account_square" /><p></td>
  <td style="text-align:center;">group_add<br><p><img src="svgs/User/group_add.svg" width="40" title="group_add" /><p></td>
  <td style="text-align:center;">group_equal<br><p><img src="svgs/User/group_equal.svg" width="40" title="group_equal" /><p></td>
  <td style="text-align:center;">group_junior<br><p><img src="svgs/User/group_junior.svg" width="40" title="group_junior" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">group_senior<br><p><img src="svgs/User/group_senior.svg" width="40" title="group_senior" /><p></td>
  <td style="text-align:center;">user_add<br><p><img src="svgs/User/user_add.svg" width="40" title="user_add" /><p></td>
  <td style="text-align:center;">user_big_outlined<br><p><img src="svgs/User/user_big_outlined.svg" width="40" title="user_big_outlined" /><p></td>
  <td style="text-align:center;">user_big<br><p><img src="svgs/User/user_big.svg" width="40" title="user_big" /><p></td>
  <td style="text-align:center;">user_outlined<br><p><img src="svgs/User/user_outlined.svg" width="40" title="user_outlined" /><p></td>
  <td style="text-align:center;">user_switch<br><p><img src="svgs/User/user_switch.svg" width="40" title="user_switch" /><p></td>
</tr>
<tr>
  <td style="text-align:center;">user<br><p><img src="svgs/User/user.svg" width="40" title="user" /><p></td>
  <td style="text-align:center;">users_switch<br><p><img src="svgs/User/users_switch.svg" width="40" title="users_switch" /><p></td>
</tr>
</table>